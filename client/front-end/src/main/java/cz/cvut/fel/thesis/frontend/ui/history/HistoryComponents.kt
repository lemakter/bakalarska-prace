package cz.cvut.fel.thesis.frontend.ui.history

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ArrowBackIos
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.mybody.SingleMapping
import cz.cvut.fel.thesis.frontend.ui.utils.NavigationVals
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextLight
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBoldLight
import cz.cvut.fel.thesis.frontend.viewmodel.UIState
import cz.cvut.fel.thesis.frontend.utilities.resultToIcon
import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping

@Composable
fun InfoRow(label: String, value: String) {
    Row(modifier = Modifier.padding(10.dp, 2.dp)) {
        DefTextSemiBoldLight(label)
        DefTextLight(value)
    }
}

@Composable
fun SingleMappingTile(mapping: Mapping, onMappingClick: (m: Mapping) -> Unit) {
    Box(
        modifier = Modifier
            .padding(vertical = 8.dp, horizontal = 8.dp)
            .fillMaxWidth()
            .clip(RoundedCornerShape(20))
            .background(MaterialTheme.colorScheme.secondary)
            .clickable { onMappingClick.invoke(mapping) }
    ) {
        Column(modifier = Modifier.padding(10.dp, 10.dp)) {
            Row {
                Column {
                    InfoRow(label = "Mapping number: ", value = mapping.mappingNumber.toString())
                    InfoRow(
                        label = "Date: ",
                        value = mapping.firstRecordDate()?.toLocalDate().toString()
                    )
                }
                Spacer(Modifier.weight(1f))
                Icon(
                    imageVector = resultToIcon(mapping.getAIResult()),
                    modifier = Modifier
                        .scale(2f)
                        .padding(15.dp),
                    contentDescription = ""
                )
            }
            InfoRow(label = "Notes: ", value = mapping.note)
        }
    }
}

@Composable
fun HistoryHome(mappings: List<Mapping>, onMappingClick: (m: Mapping) -> Unit) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        LazyColumn {
            items(items = mappings) { m ->
                SingleMappingTile(mapping = m, onMappingClick = onMappingClick)
            }
        }
    }
}

@Composable
fun SingleHistory(state: UIState, onClickBack: () -> Unit) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(modifier = Modifier.padding(15.dp)) {
            IconButton(
                onClick = onClickBack, modifier = Modifier.align(
                    Alignment.Start
                )
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Outlined.ArrowBackIos,
                    contentDescription = null
                )
            }
            SingleMapping(
                "Mapping number " + state.mainViewModel.selectedMapping.value.mappingNumber.toString(),
                state.mainViewModel.selectedMapping.value
            ) {
                state.selectedRecord = it
                state.navController.navigate(NavigationVals.SINGLE_RECORD.name)
            }
        }
    }
}
