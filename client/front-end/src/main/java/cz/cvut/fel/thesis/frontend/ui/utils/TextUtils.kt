package cz.cvut.fel.thesis.frontend.ui.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.MenuBook
import androidx.compose.material.icons.outlined.AccessibilityNew
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType

class TextUtils {

    companion object {
        val defFontSize = TextUnit(18f, TextUnitType.Sp)
        val defFontSizeHeader = TextUnit(25f, TextUnitType.Sp)

        val items = listOf(
            Pair("My Body", Icons.Outlined.AccessibilityNew),
            Pair("History", Icons.AutoMirrored.Outlined.MenuBook),
            Pair("Settings", Icons.Outlined.Settings)
        )

        @Composable
        fun DefText(text: String, modifier:Modifier=Modifier) {
            Text(text = text, modifier= modifier, fontSize = defFontSize)
        }
        @Composable
        fun DefTextLight(text: String) {
            Text(text = text, fontSize = defFontSize, color  = MaterialTheme.colorScheme.onSecondary)
        }

        @Composable
        fun DefTextSemiBold(text: String) {
            Text(text = text, fontSize = defFontSize, fontWeight = FontWeight.SemiBold)
        }

        @Composable
        fun DefTextSemiBoldLight(text: String) {
            Text(text = text, fontSize = defFontSize, fontWeight = FontWeight.SemiBold,
                color = MaterialTheme.colorScheme.onSecondary)
        }

    }
}