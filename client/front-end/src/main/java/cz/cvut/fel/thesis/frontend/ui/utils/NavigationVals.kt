package cz.cvut.fel.thesis.frontend.ui.utils

enum class NavigationVals {
    HOME_SCREEN,
    SINGLE_RECORD,
    SINGLE_HISTORY,
    SPECIFICATIONS,
    NEW_SINGLE_RECORD,
    NEW_MAPPING,
    NEW_RECORD_MAPPING,
    NEXT_RECORD_PREVIEW,
    NEW_IMAGE,
    LOG_IN,
    RELATED_RECORDS
}