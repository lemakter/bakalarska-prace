package cz.cvut.fel.thesis.frontend.ui.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.Logout
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.defFontSizeHeader
import cz.cvut.fel.thesis.frontend.viewmodel.UserViewModel
import cz.cvut.fel.thesis.logic.datamodel.AccountSettings

@Composable
fun ChangeButton(text: String, onClick: () -> Unit) {
    Row(modifier = Modifier.padding(10.dp, 10.dp)) {
        Spacer(modifier = Modifier.weight(1f))
        Button(
            onClick = onClick,
            modifier = Modifier.size(200.dp, 30.dp),
            contentPadding = PaddingValues(horizontal = 20.dp, vertical = 2.dp)
        ) {
            DefText(text)
        }
    }
}

@Composable
fun InfoRow(name: String, value: String) {
    Row {
        DefTextSemiBold(name)
        DefText(value)
    }
}

@Composable
fun InfoRowWithChangeIcon(name: String, value: String, onClick: () -> Unit) {
    Row {
        DefTextSemiBold(name)
        DefText(value)
        Spacer(modifier = Modifier.size(20.dp))
        IconButton(onClick = onClick, modifier = Modifier.size(27.dp)) {
            Icon(
                imageVector = Icons.Outlined.Edit,
                contentDescription = "Edit pencil icon",
                modifier = Modifier.size(25.dp)
            )
        }
    }
}

@Composable
fun SectionHeader(header: String) {
    Text(header, fontWeight = FontWeight.Bold, fontSize = defFontSizeHeader)
}

@Composable
fun MyDivider() {
    HorizontalDivider(
        modifier = Modifier
            .fillMaxWidth()
            .size(2.dp),
        color = MaterialTheme.colorScheme.primary
    )
    Spacer(modifier = Modifier.size(10.dp))
}

@Composable
fun LabelSwitch(name: String, check: Boolean, onChange: (Boolean) -> Unit) {
    var checked by remember { mutableStateOf(check) }
    Row {
        Spacer(modifier = Modifier.size(15.dp))
        DefText(name)
        Spacer(modifier = Modifier.size(20.dp))
        Switch(
            checked = checked,
            onCheckedChange = { checked = it; onChange.invoke(checked) },
            modifier = Modifier
                .scale(0.7f)
                .size(20.dp)
        )
    }
}

@Composable
fun SettingsPage(viewModel: UserViewModel, preferences: AccountSettings, onLogout:()->Unit) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(modifier = Modifier.padding(10.dp, 0.dp)) {
            SectionHeader(header = "Credentials")
            MyDivider()
            InfoRow(name = "Email: ", value = viewModel.cachedCredentials?.user?.email.orEmpty())
            ChangeButton(text = "Change credentials") { /*TODO*/ }
            InfoRow(name = "Password: ", value = "*******")
            ChangeButton(text = "Change password") { /*TODO*/ }
            SectionHeader(header = "Preferences")
            MyDivider()

            InfoRowWithChangeIcon(
                name = "Notification Frequency: ",
                value = preferences.notificationFrequency.toString() + " days"
            ) {/*TODO*/ }
            InfoRowWithChangeIcon(
                name = "Notification Time: ",
                value = preferences.notificationTime.toString()
            ) {/*TODO*/ }
            DefTextSemiBold(
                "Notification type: "
            )
            LabelSwitch(
                name = "Email ",
                check = preferences.notificationByMail,
                onChange = { preferences.switchByMail() })
            LabelSwitch(
                name = "Phone Notification ",
                check = preferences.notificationByPhoneNotification,
                onChange = { preferences.notificationByPhoneNotification = it })
            Row {
                DefTextSemiBold(
                    "Notify me when image is evaluated"
                )
                var checked by remember { mutableStateOf(preferences.notifyWhenEvaluated) }
                Spacer(modifier = Modifier.size(20.dp))
                Switch(
                    checked = checked,
                    onCheckedChange = { preferences.notifyWhenEvaluated = it; checked = it },
                    modifier = Modifier
                        .scale(0.7f)
                        .size(20.dp)
                )
            }
            Spacer(modifier = Modifier.size(20.dp))
            MyDivider()
            Button(onClick = {viewModel.logout(onLogout)}){
                Row {
                    DefText(text = "Log out")
                    Icon(imageVector = Icons.AutoMirrored.Outlined.Logout, contentDescription = "Logout icon")
                }
            }
        }
    }
}

