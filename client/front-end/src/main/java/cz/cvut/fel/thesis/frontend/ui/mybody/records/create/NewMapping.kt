package cz.cvut.fel.thesis.frontend.ui.mybody.records.create

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToBitmap
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord


@Composable
fun RecordCard(record: PhotoRecord) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp, 12.dp)
    ) {
        Column(modifier = Modifier.padding(12.dp, 12.dp)) {
            Row {
                DefTextSemiBold(text = "General Placement: ")
                DefText(text = record.placement.lable)
            }
            Row {
                DefTextSemiBold(text = "Description: ")
                DefText(text = record.description)
            }

            Row {
                Spacer(modifier = Modifier.weight(1f))
                Image(
                    bitmap = byteArrayToBitmap(record.img),
                    contentDescription = null,
                    modifier = Modifier
                        .requiredHeight(100.dp)
                        .requiredWidth(100.dp)
                        .clip(RoundedCornerShape(20))
                )
            }
        }
    }
}


@Composable
fun NextMappingRecord(
    next: PhotoRecord,
    onCancelMapping: () -> Unit,
    onSkipRecord: () -> Unit,
    onCreateRecord: () -> Unit,
    isNew: Boolean = false
) {
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .padding(12.dp, 12.dp),
        color = MaterialTheme.colorScheme.background
    ) {

        Column {
            if (isNew) {
                DefTextSemiBold("Let's start mapping.")
            }
            DefText("Please take a good look at previous the photo and try recreating it as precisely as possible. ")
            Row {
                DefTextSemiBold(text = if (isNew) "First " else "Next ")
                DefTextSemiBold("record is: ")
            }
            Spacer(modifier = Modifier.size(20.dp))
            RecordCard(record = next)
            val m = Modifier.align(Alignment.CenterHorizontally)
            Button(
                onClick = onCreateRecord, modifier = m
            ) {
                DefText(text = "Create Record")
            }
            Spacer(modifier = Modifier.size(10.dp))

            Button(
                onClick = onSkipRecord, modifier = m
            ) {
                DefText(text = "Skip this Record")
            }
            Spacer(modifier = Modifier.size(10.dp))
            Button(
                onClick = onCancelMapping,
                modifier = m,
                colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary)
            ) {
                DefText(text = "Cancel Mapping")
            }
        }

    }
}

@Composable
fun StartNewMapping(
    first: PhotoRecord,
    onCancelMapping: () -> Unit,
    onSkipRecord: () -> Unit,
    onCreateRecord: () -> Unit
) {
    NextMappingRecord(
        next = first,
        onCancelMapping = onCancelMapping,
        onSkipRecord = onSkipRecord,
        onCreateRecord = onCreateRecord,
        isNew = true
    )
}