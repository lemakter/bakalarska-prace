package cz.cvut.fel.thesis.frontend.ui.utils

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.viewmodel.MainViewModel

class FABUtils {

    companion object {

        @Composable
        private fun BottomSheetButton(
            text: String,
            modifier: Modifier = Modifier,
            onClick: () -> Unit
        ) {
            Button(
                onClick = onClick,
                modifier = modifier
                    .padding(16.dp, 8.dp)
                    .fillMaxWidth(0.9f)
                    .requiredHeight(77.dp),
                shape = RoundedCornerShape(25)
            ) {
                DefText(text)
            }
        }

        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun NewRecordButton(
            modifier: Modifier,
            onClickSingle: () -> Unit,
            onClickMapping: () -> Unit,
            viewModel: MainViewModel
        ) {
            var showSheet by rememberSaveable { mutableStateOf(false) }

            FloatingActionButton(modifier = modifier
                .scale(1.2f)
                .padding(24.dp), onClick = {
                showSheet = true
            }) {
                Icon(imageVector = Icons.Outlined.Add, contentDescription = "New Recofrd button")
            }
            if (showSheet) {
                ModalBottomSheet(
                    onDismissRequest = { showSheet = false },
                    sheetState = rememberModalBottomSheetState()
                ) {
                    val m = Modifier.align(Alignment.CenterHorizontally)
                    BottomSheetButton(
                        text = "Create Single Record",
                        modifier = m,
                        onClick = onClickSingle
                    )
                    if (viewModel.currentMapping.value.id != "") {
                        BottomSheetButton(
                            text = "Start New Mapping",
                            modifier = m,
                            onClick = onClickMapping
                        )
                    }
                    Spacer(modifier = Modifier.size(20.dp))
                }
            }
        }

    }
}