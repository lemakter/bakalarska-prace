package cz.cvut.fel.thesis.frontend.ui.mybody.records

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ArrowBackIos
import androidx.compose.material.icons.outlined.ArrowBackIos
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.defFontSizeHeader
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToBitmap
import cz.cvut.fel.thesis.frontend.utilities.resultToIcon
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoSpecification

@Composable
fun SpecificationItem(specification: PhotoSpecification) {
    Image(
        bitmap = byteArrayToBitmap(specification.croppedImg),
        contentDescription = null,
        modifier = Modifier
            .fillMaxWidth(0.9f)
            .size(150.dp)
    )
    Spacer(modifier = Modifier.size(10.dp))
    Row {
        Spacer(modifier = Modifier.weight(1f))
        DefTextSemiBold(text = "AI result: ")
        Spacer(modifier = Modifier.size(20.dp))
        Icon(
            imageVector = resultToIcon(specification.result),
            contentDescription = null,
            modifier = Modifier.size(40.dp)
        )
    }
    Spacer(modifier = Modifier.size(10.dp))
    HorizontalDivider(
        modifier = Modifier
            .fillMaxWidth()
            .size(2.dp),
        color = MaterialTheme.colorScheme.primary
    )

}

@Composable
fun RecordSpecifications(record: PhotoRecord, onClickBack: () -> Unit) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(modifier = Modifier.padding(20.dp, 0.dp)) {
            IconButton(
                onClick = onClickBack, modifier = Modifier.align(
                    Alignment.Start
                )
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Outlined.ArrowBackIos,
                    contentDescription = null
                )
            }
            Text(
                "Location Description: ",
                fontWeight = FontWeight.Bold,
                fontSize = defFontSizeHeader
            )
            DefText(record.description)
            Spacer(modifier = Modifier.size(10.dp))
            if (record.specifications.isEmpty()) {
                DefTextSemiBold("No lesions were identified on this record.")
            } else {
                LazyColumn {
                    items(items = record.specifications) { s ->
                        SpecificationItem(specification = s)
                    }
                }
            }

        }
    }
}