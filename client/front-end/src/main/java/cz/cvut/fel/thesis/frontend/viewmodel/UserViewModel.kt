package cz.cvut.fel.thesis.frontend.viewmodel

import android.content.Context
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.callback.Callback
import com.auth0.android.management.ManagementException
import com.auth0.android.management.UsersAPIClient
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.auth0.android.result.UserProfile
import cz.cvut.fel.thesis.R
import cz.cvut.fel.thesis.logic.client.TokenProvider

class UserViewModel : ViewModel() {

    private val tokenProvider: TokenProvider = TokenProvider.getInstance()


    private val TAG = "UserViewModel"
    private lateinit var account: Auth0
    private lateinit var context: Context

    var cachedCredentials: Credentials? = null
    private var cachedUserProfile: UserProfile? = null
    var userIsAuthenticated by mutableStateOf(false)


    private fun getUserMetadata() {
        if (cachedCredentials == null) {
            return
        }

        val usersClient = UsersAPIClient(account, cachedCredentials!!.accessToken)

        usersClient
            .getProfile(cachedUserProfile!!.getId()!!)
            .start(object : Callback<UserProfile, ManagementException> {

                override fun onFailure(exception: ManagementException) {
                    Log.d(TAG, "Error while trying to retrive user data.", exception)
                }

                override fun onSuccess(userProfile: UserProfile) {
                    cachedUserProfile = userProfile
                }

            })
    }

    private fun setUserMetadata(value: String) {
        if (userIsAuthenticated == false) {
            return
        }

        val usersClient = UsersAPIClient(account, cachedCredentials!!.accessToken)
        val metadata = mapOf("country" to value)

        usersClient
            .updateMetadata(cachedUserProfile!!.getId()!!, metadata)
            .start(object : Callback<UserProfile, ManagementException> {

                override fun onFailure(exception: ManagementException) {
                    Log.d(TAG, "Error setting user data.", exception)
                }

                override fun onSuccess(profile: UserProfile) {
                    cachedUserProfile = profile
                }

            })
    }


    fun setContext(activityContext: Context) {
        context = activityContext
        account = Auth0(
            context.getString(R.string.com_auth0_client_id),
            context.getString(R.string.com_auth0_domain)
        )
    }

    // https://auth0.com/blog/android-authentication-jetpack-compose-part-2/
    fun login() {
        WebAuthProvider
            .login(account)
            .withScheme(context.getString(R.string.com_auth0_scheme))
            .start(context, object : Callback<Credentials, AuthenticationException> {

                override fun onFailure(error: AuthenticationException) {
                    Log.e(TAG, "Error occurred in login(): $error")
                }

                override fun onSuccess(creadentials: Credentials) {
                    val token = creadentials.idToken
                    Log.d(TAG, "ID token: $token")

                    userIsAuthenticated = true
                    tokenProvider.token = token
                    cachedCredentials = creadentials
                }
            })
    }

    fun logout(onLogout: () -> Unit) {
        WebAuthProvider
            .logout(account)
            .withScheme(context.getString(R.string.com_auth0_scheme))
            .start(context, object : Callback<Void?, AuthenticationException> {

                override fun onFailure(error: AuthenticationException) {
                    Log.e(TAG, "Error occurred in logout(): $error")
                }

                override fun onSuccess(result: Void?) {
                    userIsAuthenticated = false
                    tokenProvider.token = ""
                    onLogout.invoke()
                }
            })
    }
}