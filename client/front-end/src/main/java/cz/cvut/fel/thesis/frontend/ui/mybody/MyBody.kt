package cz.cvut.fel.thesis.frontend.ui.mybody

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ExpandLess
import androidx.compose.material.icons.outlined.ExpandMore
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import cz.cvut.fel.thesis.frontend.ui.utils.FABUtils.Companion.NewRecordButton
import cz.cvut.fel.thesis.frontend.ui.utils.NavigationVals
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.defFontSizeHeader
import cz.cvut.fel.thesis.frontend.viewmodel.UIState
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToBitmap
import cz.cvut.fel.thesis.frontend.utilities.resultToIcon
import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord

@Composable
fun RecordPreview(record: PhotoRecord, onClick: () -> Unit) {
    Button(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.secondary),
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp, 2.dp),
        shape = RoundedCornerShape(10)
    ) {
        Column(modifier = Modifier.padding(8.dp, 8.dp)) {
            Row {
                Text(
                    "Description: ",
                    fontWeight = FontWeight.Bold,
                    fontSize = TextUtils.defFontSize
                )
                DefText(record.description)
            }
            Row {
                Image(
                    modifier = Modifier
                        .size(100.dp)
                        .clip(RoundedCornerShape(20)),
                    bitmap = byteArrayToBitmap(record.img),
                    contentDescription = "record img"
                )
                Spacer(Modifier.weight(1f))
                Column {
                    DefText("AI result: ")
                    Image(
                        modifier = Modifier.size(50.dp),
                        imageVector = resultToIcon(record.result),
                        contentDescription = "result img"
                    )
                }
            }
        }
    }
}

@Composable
fun ExpandedItem(
    mapping: Mapping,
    placement: Placement,
    onRecordClick: (PhotoRecord) -> Unit
) {
    val records = placement.recordsFor(mapping)
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier
            .fillMaxWidth(0.95f)

    ) {
        Column(
            modifier = Modifier
                .padding(1.dp, 1.dp)
                .fillMaxWidth()
        ) {
            records.forEach { r ->
                RecordPreview(record = r) { onRecordClick(r) }
            }
        }
    }
}

@Composable
fun InfoDialogCard(mapping: Mapping) {
    Card(
        modifier = Modifier
            .fillMaxWidth(), shape = RoundedCornerShape(5)
    ) {
        Column(modifier = Modifier.padding(15.dp, 15.dp)) {
            Row {
                DefText(text = "Date of ")
                DefTextSemiBold(text = "Newest ")
                DefText(text = "Record: ")
                DefText(text = mapping.firstRecordDate()?.toLocalDate().toString())
            }
            Row {
                DefText(text = "Date of ")
                DefTextSemiBold(text = "Oldest ")
                DefText(text = "Record: ")
                DefText(text = mapping.lastRecordDate()?.toLocalDate().toString())
            }
            Row {
                DefTextSemiBold(text = "Mapping number: ")
                DefText(text = mapping.mappingNumber.toString())
            }
            Row(modifier = Modifier.padding()) {
                DefTextSemiBold(text = "Number of records: ")
                DefText(text = mapping.getRecords().size.toString())
            }
            Row {
                DefTextSemiBold(text = "Mapping notes: ")
                DefText(text = mapping.note)
            }
        }
    }
}

@Composable
fun ListItemExpandable(
    mapping: Mapping,
    placement: Placement,
    onRecordClick: (PhotoRecord) -> Unit,
) {
    var expanded by rememberSaveable { mutableStateOf(false) }
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier
            .padding(vertical = 4.dp, horizontal = 4.dp)
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Surface(
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier
                    .clip(CircleShape)
                    .clickable { expanded = !expanded }
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Text(
                        text = placement.lable,
                        fontSize = defFontSizeHeader,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier.padding(15.dp, 10.dp)
                    )
                    Spacer(Modifier.weight(1f))

                    Icon(
                        if (expanded) Icons.Outlined.ExpandLess else Icons.Outlined.ExpandMore,
                        contentDescription = "Localized description",
                        modifier = Modifier.padding(15.dp)
                    )

                }
            }
            if (expanded) {
                ExpandedItem(
                    mapping = mapping,
                    placement = placement,
                    onRecordClick = onRecordClick
                )
            }

        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SingleMapping(
    header: String,
    mapping: Mapping,
    onRecordClick: (PhotoRecord) -> Unit
) {
    var showInfo by rememberSaveable { mutableStateOf(false) }
    Column {
        Row {
            Text(
                text = header,
                fontSize = defFontSizeHeader,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier.padding(10.dp)
            )
            Spacer(modifier = Modifier.weight(1f))
            IconButton(
                onClick = { showInfo = !showInfo },
                modifier = Modifier
                    .scale(1.5f)
                    .align(Alignment.Top)
            ) {
                Icon(
                    imageVector = Icons.Outlined.Info,
                    contentDescription = "Info Icon"
                )
            }
        }
        Row(modifier = Modifier.padding(10.dp)) {
            LazyColumn {
                items(items = mapping.nonemptyPlacements()) { p ->
                    ListItemExpandable(
                        mapping = mapping,
                        placement = p,
                        onRecordClick = onRecordClick
                    )
                }
            }
        }
    }
    if (showInfo) {
        BasicAlertDialog(
            onDismissRequest = { showInfo = false },
            modifier = Modifier.fillMaxWidth(),
            properties = DialogProperties()
        ) {
            InfoDialogCard(mapping)
        }
    }
}

@Composable
fun MyBodyHome(mapping: Mapping, state: UIState) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp)
    ) {
        Box {
            SingleMapping(
                header = "My body",
                mapping = mapping, onRecordClick = {
                    state.selectedRecord = it
                    state.navController.navigate(NavigationVals.SINGLE_RECORD.name)
                }
            )
            NewRecordButton(
                modifier = Modifier.align(Alignment.BottomEnd),
                onClickSingle = {
                    state.navController.navigate(
                        NavigationVals.NEW_SINGLE_RECORD.name
                    )
                },
                onClickMapping = {
                    state.nextRecordIdx = 0
                    state.recordsList = ArrayList()
                    state.navController.navigate(
                        NavigationVals.NEW_MAPPING.name
                    )
                },
                viewModel = state.mainViewModel
            )
        }
    }
}
