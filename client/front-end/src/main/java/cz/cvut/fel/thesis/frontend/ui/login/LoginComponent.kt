package cz.cvut.fel.thesis.frontend.ui.login

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.viewmodel.UserViewModel


@Composable
fun LoginPage(onAuthenticated: () -> Unit, viewModel: UserViewModel) {
    Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
        Column(modifier = Modifier.padding(20.dp)) {
            DefText(
                text = "By logging into this application I consent to images or my body being stored on an external server.",
                modifier = Modifier.padding(15.dp)
            )
            HorizontalDivider(color = Color.LightGray)
            DefText(
                text = "I understand that this service is not a valid substitute for dermatological checkups, and  the evaluations provided by AI are for information purposes only and may not be accurate.",
                modifier = Modifier.padding(15.dp)
            )
            Spacer(modifier = Modifier.size(30.dp))
            Button(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = { viewModel.login() }) {
                DefText(text = "Let's start")
            }
        }
    }
    if (viewModel.userIsAuthenticated) {
        onAuthenticated.invoke()
    }
}
