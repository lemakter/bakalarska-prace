package cz.cvut.fel.thesis.frontend.ui.utils

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import cz.cvut.fel.thesis.frontend.ui.history.HistoryHome
import cz.cvut.fel.thesis.frontend.ui.mybody.MyBodyHome
import cz.cvut.fel.thesis.frontend.ui.settings.SettingsPage
import cz.cvut.fel.thesis.frontend.viewmodel.UIState

@Composable
fun HomeScreen(state: UIState, onLogout:()->Unit) {
    var selectedItem: Int by rememberSaveable { mutableIntStateOf(state.selected) }
    val settingsService = state.settingsService
    Scaffold(
        bottomBar = {
            NavigationBar {
                TextUtils.items.forEachIndexed { index, item ->
                    NavigationBarItem(
                        icon = {
                            Icon(
                                imageVector = item.second,
                                contentDescription = item.first
                            )
                        },
                        label = { TextUtils.DefText(item.first) },
                        selected = selectedItem == index,
                        onClick = { selectedItem = index }
                    )
                }
            }
        }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colorScheme.background
        ) {
            when (selectedItem) {
                0 -> {
                    MyBodyHome(mapping = state.mainViewModel.currentMapping.value, state = state)
                }

                1 -> {
                    state.mainViewModel.getChronological()
                    HistoryHome(
                        state.mainViewModel.allChronological
                    ) { m ->
                        state.mainViewModel.selectedMapping.value = m
                        state.navController.navigate(NavigationVals.SINGLE_HISTORY.name)
                    }
                }

                2 -> {
                    SettingsPage(state.userViewModel, settingsService.getUserSettings(), onLogout)
                }
            }
        }
    }
}
