package cz.cvut.fel.thesis.frontend.ui.mybody.records

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ArrowBackIos
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import cz.cvut.fel.thesis.frontend.viewmodel.MainViewModel
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.defFontSizeHeader
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToBitmap
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord

@Composable
fun RecordButton(text: String, modifier: Modifier, onClick: () -> Unit) {
    Spacer(modifier = Modifier.size(7.dp))
    Button(
        onClick = onClick,
        modifier = modifier, contentPadding = PaddingValues(horizontal = 20.dp, vertical = 2.dp)

    ) {
        DefText(text = text)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SingleRecord(
    record: PhotoRecord,
    onClickBack: () -> Unit,
    onClickSpecification: () -> Unit,
    onClickNewImage: (PhotoRecord) -> Unit,
    viewModel: MainViewModel,
    onClickRelated: (PhotoRecord) -> Unit
) {
    var showInfo by remember { mutableStateOf(false) }
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .fillMaxSize()
    ) {
        Box(modifier = Modifier.padding(20.dp, 0.dp))
        {
            Column(modifier = Modifier.fillMaxSize()) {
                Row {
                    IconButton(
                        onClick = onClickBack
                    )
                    {
                        Icon(
                            imageVector = Icons.AutoMirrored.Outlined.ArrowBackIos,
                            contentDescription = null
                        )
                    }
                    Text(
                        record.placement.lable,
                        fontWeight = FontWeight.Bold,
                        fontSize = defFontSizeHeader,
                        modifier = Modifier.padding(10.dp)
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    IconButton(
                        onClick = { showInfo = true },
                        modifier = Modifier
                            .scale(1.5f)
                            .align(Alignment.Top)
                    )
                    {
                        Icon(
                            imageVector = Icons.Outlined.Info,
                            contentDescription = null
                        )
                    }
                }
                Text(
                    "Description: ",
                    fontWeight = FontWeight.SemiBold,
                    fontSize = defFontSizeHeader
                )
                DefText(record.description)
                var result by remember { mutableStateOf(false) }
                Image(
                    bitmap = if (result) byteArrayToBitmap(record.responseImg)
                    else byteArrayToBitmap(record.img),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(20.dp, 20.dp)
                        .height(300.dp)
                        .fillMaxWidth()
                        .clickable { onClickSpecification.invoke() },
                    alignment = Alignment.TopCenter,
                )
                if (record.responseImg != null) {
                    Row(modifier = Modifier.padding(0.dp,10.dp)) {
                        DefText(text = "See detected lesions: ")
                        Spacer(modifier = Modifier.size(15.dp))
                        Switch(
                            checked = result,
                            onCheckedChange = { result = !result },
                            modifier = Modifier
                                .scale(1f)
                                .size(20.dp)
                        )
                    }
                }
                Row {
                    DefTextSemiBold("AI result: ")
                    DefText(record.getAIResult().verbal)
                }
                val modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .size(280.dp, 40.dp)
                RecordButton(
                    text = "Discontinue",
                    modifier = modifier
                ) { viewModel.discontinue(record) }
                RecordButton(
                    text = "Rerun AI analysis",
                    modifier = modifier
                ) { viewModel.rerunAnalysis(record) }
                RecordButton(
                    text = "New image for this location",
                    modifier = modifier
                ) { onClickNewImage(record) }
            }
        }
        if (showInfo) {
            BasicAlertDialog(
                onDismissRequest = { showInfo = false },
                modifier = Modifier.fillMaxWidth(),
                properties = DialogProperties()
            ) {
                RecordInfoDialogCard(record, onClickRelated)
            }
        }
    }
}

@Composable
fun RecordInfoDialogCard(r: PhotoRecord, onClickRelated: (PhotoRecord) -> Unit) {
    Card(
        shape = RoundedCornerShape(5)
    ) {
        Column(modifier = Modifier.padding(15.dp)) {

            Row {
                DefTextSemiBold(text = "Created on: ")
                DefText(text = r.created.toLocalDate()?.toString().orEmpty())
            }
            Row {
                DefTextSemiBold(text = "Number of related records: ")
                DefText(text = r.relatedNumber.toString())
            }
            Spacer(modifier = Modifier.size(15.dp))
            if (r.relatedNumber > 0) {
                Button(
                    onClick = { onClickRelated.invoke(r) },
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                ) {
                    DefText(text = "See related records")
                }
            }
        }
    }
}
