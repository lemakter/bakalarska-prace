package cz.cvut.fel.thesis.frontend.ui.mybody.records.create

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ExpandMore
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import cz.cvut.fel.thesis.BuildConfig
import cz.cvut.fel.thesis.frontend.ui.settings.MyDivider
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.defFontSizeHeader
import cz.cvut.fel.thesis.frontend.viewmodel.UIState
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToCompressedBitmap
import cz.cvut.fel.thesis.frontend.utilities.getContentData
import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Objects


/**
 * https://medium.com/@dheerubhadoria/capturing-images-from-camera-in-android-with-jetpack-compose-a-step-by-step-guide-64cd7f52e5de
 */
fun Context.createImageFile(): File {
    // Create an image file name
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    return File.createTempFile(
        imageFileName, /* prefix */
        ".jpg", /* suffix */
        externalCacheDir      /* directory */
    )
}

@RequiresApi(Build.VERSION_CODES.P)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UploadImageDialog(
    modifier: Modifier = Modifier,
    onDismiss: () -> Unit,
    onPhoto: (ByteArray) -> Unit,
) {

    val context = LocalContext.current
    val file = context.createImageFile()
    val uri = FileProvider.getUriForFile(
        Objects.requireNonNull(context),
        BuildConfig.APPLICATION_ID + ".provider", file
    )
    var capturedImageUri by remember {
        mutableStateOf<Uri>(Uri.EMPTY)
    }
    val cameraLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) {
            capturedImageUri = uri
        }
    val permissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {
        if (it) {
            Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
            cameraLauncher.launch(uri)
        } else {
            Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }

    val mediaPickerLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.PickVisualMedia()) {
            if (it != null) capturedImageUri = it
        }

    BasicAlertDialog(
        onDismissRequest = onDismiss,
        modifier = modifier
            .fillMaxWidth(),
        properties = DialogProperties()
    ) {
        Card(
            modifier = modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(20))
        ) {
            Column(
                modifier = Modifier
                    .padding(12.dp, 12.dp)
                    .align(Alignment.CenterHorizontally)

            ) {

                Button(
                    onClick = {
                        val permissionCheckResult =
                            ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                        if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
                            cameraLauncher.launch(uri)
                        } else {
                            // Request a permission
                            permissionLauncher.launch(Manifest.permission.CAMERA)
                        }

                    }) {
                    DefText(text = "Take a photo")
                }
                Button(
                    onClick = {
                        mediaPickerLauncher.launch(
                            PickVisualMediaRequest(mediaType = ActivityResultContracts.PickVisualMedia.ImageOnly)
                        )
                    }
                )
                {
                    DefText(text = "Use existing")
                }

            }
        }
    }
    if (capturedImageUri.path?.isNotEmpty() == true) {
        onPhoto.invoke(getContentData(context, capturedImageUri))
    }
}


@RequiresApi(Build.VERSION_CODES.P)
@Composable
fun CreateSingleRecord(
    onClickBack: () -> Unit,
    onClickNext: () -> Unit = onClickBack,
    defRecord: PhotoRecord?,
    state: UIState,
    newMapping: Boolean,
    fromExisting: Boolean = false
) {
    var image by remember { mutableStateOf(ByteArray(0)) }

    var img by remember { mutableStateOf(false) }
    var failed by remember { mutableStateOf(false) }
    var displayNoPlacement by remember { mutableStateOf(false) }
    var next by remember { mutableStateOf(false) }
    var navigated by remember { mutableStateOf(false) }

    var description by remember { mutableStateOf(defRecord?.description ?: "") }

    val placements = Placement.entries.toTypedArray()
    var selectedPlacement: Int by remember {
        mutableIntStateOf(
            if (defRecord == null) -1 else placements.indexOf(
                defRecord.placement
            )
        )
    }

    Surface(color = MaterialTheme.colorScheme.background, modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier.padding(20.dp, 12.dp)) {
            Text(
                "New Record",
                fontSize = defFontSizeHeader,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(0.dp, 6.dp)
            )
            MyDivider()
            var expanded by remember { mutableStateOf(false) }
            DefTextSemiBold("General placement: ")
            Button(
                onClick = { expanded = true },
                colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary)
            ) {
                Row {
                    DefText(text = if (selectedPlacement == -1) "Select placement" else placements[selectedPlacement].lable)
                    Spacer(modifier = Modifier.size(20.dp))
                    Icon(imageVector = Icons.Outlined.ExpandMore, contentDescription = null)
                }
            }
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier
                    .clip(RoundedCornerShape(15))
            ) {
                placements.forEachIndexed { i, p ->
                    DropdownMenuItem(
                        text = { DefText(p.lable) },
                        onClick = { selectedPlacement = i; expanded = false })
                }
            }
            Spacer(modifier = Modifier.size(20.dp))
            DefTextSemiBold(text = "Placement description: ")
            TextField(
                value = description,
                onValueChange = { description = it },
                modifier = Modifier.clip(
                    RoundedCornerShape(15)
                )
            )
            if (img) {
                Spacer(modifier = Modifier.size(15.dp))
                Image(
                    bitmap = byteArrayToCompressedBitmap(image),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxHeight(0.5f)
                )
            }
            Spacer(modifier = Modifier.size(15.dp))
            var dialog by remember { mutableStateOf(false) }
            Button(
                onClick = { dialog = true },
                colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary)
            ) {
                DefText("Upload Image")
            }
            if (dialog) {
                UploadImageDialog(
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally),
                    onDismiss = { dialog = false },
                    onPhoto = { image = it; img = true; dialog = false })
            }
            Spacer(modifier = Modifier.weight(1f))
            if (img) {
                var clicked by remember { mutableStateOf(false) }
                Button(
                    onClick = {
                        if (selectedPlacement == -1) {
                            displayNoPlacement = true
                        } else {
                            clicked = true
                            onEvalImage(
                                prevId = defRecord?.id.orEmpty(),
                                image = image,
                                description = if (description.length > 600) "" else description, //checking for suspiciously long description
                                placement = placements[selectedPlacement],
                                state = state,
                                onFail = { img = false; failed = true; clicked = false },
                                onSuccess = { next = true },
                                newMapping = newMapping,
                                fromExisting = fromExisting
                            )
                        }
                    },
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    enabled = !clicked
                )
                {
                    if (next && !navigated) {
                        navigated = true
                        onClickNext.invoke()
                    }
                    DefText(text = "Check Image Quality")
                }
            }
            Button(
                onClick = onClickBack,
                modifier = Modifier.align(Alignment.CenterHorizontally),
                colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.secondary)
            ) {
                DefText(text = "Cancel")
            }
        }
    }
    if (failed) {
        ErrorDialog(text = "Image quality is insufficient. Please create or choose a new image.") {
            failed = false
            navigated = false
        }
    }
    if (displayNoPlacement) {
        ErrorDialog(text = "Please fill out the placement before creating a record. ") {
            displayNoPlacement = false
        }
    }
}

private fun onEvalImage(
    prevId: String,
    image: ByteArray,
    description: String,
    placement: Placement,
    state: UIState,
    onFail: () -> Unit,
    onSuccess: () -> Unit,
    newMapping: Boolean,
    fromExisting: Boolean
) {
    state.mainViewModel.evalImg(image, onSuccess = {
        onSuccess.invoke()
        val rec = PhotoRecord(
            placement = placement,
            description = description,
            imgId = state.mainViewModel.image.value
        )
        if (newMapping) {
            rec.img = null //Remove the actual image, to conserve space
            state.recordsList.add(Pair(prevId, rec))
        } else if (fromExisting) {
            state.mainViewModel.createNewRecordFromExisting(rec, state.selectedRecord)
        } else {
            state.mainViewModel.createNewRecord(rec)
        }
    }, onFail = onFail)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ErrorDialog(text: String, onDismiss: () -> Unit) {
    BasicAlertDialog(
        onDismissRequest = onDismiss,
        modifier = Modifier.fillMaxWidth(),
        properties = DialogProperties()
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth(), shape = RoundedCornerShape(5)
        ) {
            Column(modifier = Modifier.padding(20.dp)) {
                DefText(text = text)
                Button(
                    onClick = onDismiss,
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                ) {
                    DefText(text = "Dismiss")
                }
            }
        }
    }
}