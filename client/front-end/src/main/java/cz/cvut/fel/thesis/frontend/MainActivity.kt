package cz.cvut.fel.thesis.frontend

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import cz.cvut.fel.thesis.frontend.ui.history.SingleHistory
import cz.cvut.fel.thesis.frontend.ui.login.LoginPage
import cz.cvut.fel.thesis.frontend.ui.mybody.records.RecordSpecifications
import cz.cvut.fel.thesis.frontend.ui.mybody.records.RelatedRecords
import cz.cvut.fel.thesis.frontend.ui.mybody.records.SingleRecord
import cz.cvut.fel.thesis.frontend.ui.mybody.records.create.CreateSingleRecord
import cz.cvut.fel.thesis.frontend.ui.mybody.records.create.NextMappingRecord
import cz.cvut.fel.thesis.frontend.ui.mybody.records.create.StartNewMapping
import cz.cvut.fel.thesis.frontend.ui.utils.HomeScreen
import cz.cvut.fel.thesis.frontend.viewmodel.MainViewModel
import cz.cvut.fel.thesis.frontend.ui.utils.NavigationVals
import cz.cvut.fel.thesis.frontend.viewmodel.UIState
import cz.cvut.fel.thesis.frontend.viewmodel.UserViewModel
import cz.cvut.fel.thesis.logic.service.SettingsServiceDummy


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mainViewModel: MainViewModel by viewModels()
        val userViewModel: UserViewModel by viewModels()
        userViewModel.setContext(this)
        setContent {
            MyApp(
                state = UIState(
                    settingsService = SettingsServiceDummy(),
                    mainViewModel = mainViewModel,
                    userViewModel = userViewModel
                )
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    @Composable
    fun MyApp(
        navController: NavHostController = rememberNavController(),
        state: UIState
    ) {
        state.navController = navController
        NavHost(
            navController = navController, startDestination = NavigationVals.LOG_IN.name
        ) {
            composable(route = NavigationVals.LOG_IN.name) {
                LoginPage(
                    onAuthenticated = {
                        state.selected = 0
                        state.mainViewModel.currentChanged = true
                        navController.navigate(NavigationVals.HOME_SCREEN.name)
                    },
                    viewModel = state.userViewModel
                )
            }
            composable(route = NavigationVals.HOME_SCREEN.name) {
                state.mainViewModel.getCurrentMapping()
                HomeScreen(state = state, onLogout = {
                    state.mainViewModel.logoutClear()
                    navController.navigate(NavigationVals.LOG_IN.name)
                })
            }
            composable(route = NavigationVals.SINGLE_RECORD.name) {
                SingleRecord(
                    record = state.selectedRecord,
                    onClickBack = {
                        state.selected = 0; navController.navigate(NavigationVals.HOME_SCREEN.name)
                    },
                    onClickSpecification = { navController.navigate(NavigationVals.SPECIFICATIONS.name) },
                    onClickNewImage = {
                        state.selectedRecord =
                            it; navController.navigate(NavigationVals.NEW_IMAGE.name)
                    }, viewModel = state.mainViewModel,
                    onClickRelated = { r ->
                        state.selectedRecord = r; navController.navigate(
                        NavigationVals.RELATED_RECORDS.name
                    )
                    }
                )
            }
            composable(route = NavigationVals.SINGLE_HISTORY.name) {
                state.mainViewModel.retrieveSelected(true)
                SingleHistory(
                    state = state,
                    onClickBack = {
                        state.selected = 1; navController.navigate(NavigationVals.HOME_SCREEN.name)
                    })
            }
            composable(route = NavigationVals.SPECIFICATIONS.name) {
                RecordSpecifications(
                    record = state.selectedRecord,
                    onClickBack = { navController.navigate(NavigationVals.SINGLE_RECORD.name) })
            }
            composable(route = NavigationVals.NEW_SINGLE_RECORD.name) {
                CreateSingleRecord(
                    onClickBack = {
                        state.selected = 0; navController.navigate(NavigationVals.HOME_SCREEN.name)
                    },
                    defRecord = null,
                    state = state,
                    newMapping = false
                )
                //TODO add "do you wish to discard your info dialog
            }
            composable(route = NavigationVals.NEW_IMAGE.name) {
                CreateSingleRecord(
                    onClickBack = {
                        state.selected = 0;navController.navigate(NavigationVals.HOME_SCREEN.name)
                    },
                    defRecord = state.selectedRecord,
                    state = state,
                    newMapping = false,
                    fromExisting = true
                )
            }
            composable(route = NavigationVals.NEW_MAPPING.name) {
                state.mainViewModel.getChronological()
                state.mainViewModel.retrieveSelected(false)
                val records = state.mainViewModel.selectedMapping.value.getRecords()
                if (state.nextRecordIdx != -1 && records.size > state.nextRecordIdx) {
                    val curr = records[state.nextRecordIdx]
                    StartNewMapping(
                        first = curr,
                        onCancelMapping = {
                            state.selected = 0
                            navController.navigate(
                                NavigationVals.HOME_SCREEN.name
                            )
                        },
                        onCreateRecord = { navController.navigate(NavigationVals.NEW_RECORD_MAPPING.name) },
                        onSkipRecord = {
                            state.nextRecordIdx++
                            navController.navigate(NavigationVals.NEXT_RECORD_PREVIEW.name)
                        }
                    )
                } else {
                    state.recordsList = ArrayList()
                }
            }
            composable(route = NavigationVals.NEXT_RECORD_PREVIEW.name) {
                val records = state.mainViewModel.currentMapping.value.getRecords()
                if (state.nextRecordIdx > -1 && records.size > state.nextRecordIdx) {
                    NextMappingRecord(
                        next = records[state.nextRecordIdx],
                        onCancelMapping = {
                            state.selected = 0
                            navController.navigate(NavigationVals.HOME_SCREEN.name)
                        },
                        onCreateRecord = { navController.navigate(NavigationVals.NEW_RECORD_MAPPING.name) },
                        onSkipRecord = {
                            state.nextRecordIdx++
                            navController.navigate(NavigationVals.NEXT_RECORD_PREVIEW.name)
                        }
                    )
                } else {
                    //TODO - finish mapping screen
                    if (state.nextRecordIdx != -1 && state.recordsList.isNotEmpty()) {
                        state.nextRecordIdx = -1

                        state.mainViewModel.createNewMapping(state.recordsList) //prolly if more than one record
                        state.selected = 0
                        navController.navigate(NavigationVals.HOME_SCREEN.name)
                    } else {
                        Log.d(null, "Trying to create a new mapping with no new records...")
                    }
                }
            }
            composable(route = NavigationVals.NEW_RECORD_MAPPING.name) {
                val records = state.mainViewModel.currentMapping.value.getRecords()
                if (state.nextRecordIdx > -1 && records.size > state.nextRecordIdx) {
                    val r = records[state.nextRecordIdx]
                    CreateSingleRecord(
                        onClickBack = {
                            state.selected = 0
                            navController.navigate(NavigationVals.HOME_SCREEN.name)
                        },
                        onClickNext = {
                            state.nextRecordIdx++
                            navController.navigate(NavigationVals.NEXT_RECORD_PREVIEW.name)
                        },
                        defRecord = r,
                        state = state, newMapping = true
                    )
                } else {
                    //TODO - finish mapping screen
                    if (state.nextRecordIdx != -1 && state.recordsList.isNotEmpty()) {
                        state.nextRecordIdx = -1
                        state.mainViewModel.createNewMapping(state.recordsList) //first
                        state.selected = 0
                        navController.navigate(NavigationVals.HOME_SCREEN.name)
                    } else {
                        Log.d(null, "Trying to create a new mapping with no new records...")
                    }
                }
            }
            composable(route = NavigationVals.RELATED_RECORDS.name) {
                state.mainViewModel.retrieveRelatedRecords(state.selectedRecord)
                RelatedRecords(viewModel = state.mainViewModel) {
                    state.mainViewModel.relatedRecords = ArrayList()
                    navController.navigate(NavigationVals.SINGLE_RECORD.name)
                }
            }
        }
    }
}