package cz.cvut.fel.thesis.frontend.viewmodel

import androidx.navigation.NavController
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.service.SettingsService

class UIState(
    val settingsService: SettingsService,
    val mainViewModel: MainViewModel,
    val userViewModel: UserViewModel
) {
    lateinit var selectedRecord: PhotoRecord
    lateinit var navController: NavController
    var nextRecordIdx: Int = -1
    var recordsList = ArrayList<Pair<String, PhotoRecord>>()
    var selected = 0
}