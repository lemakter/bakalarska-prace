package cz.cvut.fel.thesis.frontend.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.thesis.logic.client.RetrofitInstance
import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.service.MappingService
import cz.cvut.fel.thesis.logic.service.RecordService
import cz.cvut.fel.thesis.logic.service.impl.MappingServiceImpl
import cz.cvut.fel.thesis.logic.service.impl.RecordServiceImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel : ViewModel() {

    private val photoController = RetrofitInstance.photoController
    private val mappingController = RetrofitInstance.mappingController
    private val recordController = RetrofitInstance.recordController
    private val recordService: RecordService = RecordServiceImpl(photoController, recordController)
    private val mappingService: MappingService =
        MappingServiceImpl(mappingController, recordService)


    private val TAG = "MainViewModel"


    var image: MutableState<String> = mutableStateOf("")

    var selectedMapping = mutableStateOf(Mapping(""))
    var currentMapping = mutableStateOf(Mapping(""))
    var allChronological: List<Mapping> by mutableStateOf(listOf())
    var relatedRecords: List<PhotoRecord> by mutableStateOf(listOf())

    var currentChanged by mutableStateOf(true)

    // This function servers to drop all user information after a logout.
    fun logoutClear() {
        image.value = ""
        selectedMapping.value = Mapping("")
        currentMapping.value = Mapping("")
        allChronological = listOf()
        relatedRecords = ArrayList()
        currentChanged = true
    }

    fun evalImg(img: ByteArray, onFail: () -> Unit, onSuccess: () -> Unit) {
        image.value = ""
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                image.value = recordService.evalImage(img)
                if (image.value != "") {
                    onSuccess.invoke()
                } else {
                    onFail.invoke()
                }

            }
        }
    }

    fun getCurrentMapping() {
        if (currentChanged || currentMapping.value.id == "") {
            currentChanged = false
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    try {
                        val m = mappingService.currentMapping()
                        if (m != null) {
                            currentMapping.value = m
                            selectedMapping.value = m
                        }
                    } catch (e: Exception) {
                        Log.d(TAG, e.message.orEmpty())
                    }
                }
            }
        }
    }

    fun getChronological() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                allChronological = mappingService.findAllChronological()
            }
        }
    }

    fun retrieveSelected(withDiscontinued: Boolean) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                selectedMapping.value =
                    mappingService.getMapping(selectedMapping.value.id, withDiscontinued)
                        ?: Mapping("")
            }
        }
    }

    fun createNewRecord(r: PhotoRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                recordService.reviewRecord(r)
                currentChanged = true
            }
        }
    }

    fun createNewRecordFromExisting(r: PhotoRecord, ex: PhotoRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                recordService.newFromExisting(r, ex)
                currentChanged = true
            }
        }
    }

    fun createNewMapping(r: List<Pair<String, PhotoRecord>>) {
        if (!currentChanged) {
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    recordService.batchReview(r)
                    currentChanged = true
                }
            }
        } else {
            Log.d(TAG, "Trying to create new mapping illegally. ")
        }
    }

    fun discontinue(r: PhotoRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                recordService.discontinue(r)
                currentChanged = true
            }
        }
    }

    fun rerunAnalysis(r: PhotoRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                recordService.rerunAnalysis(r)
                currentChanged = true
            }
        }
    }

    fun retrieveRelatedRecords(r: PhotoRecord) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                relatedRecords = recordService.retrieveRelatedRecords(r)
                    .sortedByDescending { it.created }
            }
        }
    }
}