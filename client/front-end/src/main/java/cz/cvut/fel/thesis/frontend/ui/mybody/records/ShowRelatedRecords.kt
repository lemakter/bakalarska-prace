package cz.cvut.fel.thesis.frontend.ui.mybody.records

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ArrowBackIos
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cz.cvut.fel.thesis.frontend.viewmodel.MainViewModel
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefText
import cz.cvut.fel.thesis.frontend.ui.utils.TextUtils.Companion.DefTextSemiBold
import cz.cvut.fel.thesis.frontend.utilities.byteArrayToBitmap
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord

@Composable
fun RelatedRecordsItem(record: PhotoRecord) {
    Image(
        bitmap = byteArrayToBitmap(record.img),
        contentDescription = null,
        modifier = Modifier.fillMaxWidth(0.9f).size(300.dp)
    )
    Spacer(modifier = Modifier.size(10.dp))
    Row {
        DefTextSemiBold(text = "Description: ")
        DefText(text = record.description)
    }
    Row {
        DefTextSemiBold(text = "Created on: ")
        DefText(text = record.created.toLocalDate().toString())
    }
    Spacer(modifier = Modifier.size(10.dp))
    HorizontalDivider(
        modifier = Modifier
            .fillMaxWidth()
            .size(2.dp),
        color = MaterialTheme.colorScheme.primary
    )
    Spacer(modifier = Modifier.size(10.dp))
}

@Composable
fun RelatedRecords(viewModel: MainViewModel, onClickBack: () -> Unit) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(modifier = Modifier.padding(20.dp, 0.dp)) {
            IconButton(
                onClick = { viewModel.relatedRecords = listOf(); onClickBack.invoke() },
                modifier = Modifier.align(Alignment.Start)
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Outlined.ArrowBackIos,
                    contentDescription = null
                )
            }
            var records: List<PhotoRecord> by remember { mutableStateOf(listOf()) }
            records = viewModel.relatedRecords
            if (records.isEmpty()) {
                DefTextSemiBold("No related records were found for this record.")
            } else {
                LazyColumn {
                    items(items = records) { r ->
                        RelatedRecordsItem(record = r)
                    }
                }
            }

        }
    }
}