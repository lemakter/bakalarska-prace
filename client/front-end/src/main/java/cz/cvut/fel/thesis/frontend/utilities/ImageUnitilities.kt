package cz.cvut.fel.thesis.frontend.utilities

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.CheckCircle
import androidx.compose.material.icons.outlined.PriorityHigh
import androidx.compose.material.icons.outlined.QuestionMark
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import java.io.ByteArrayOutputStream


fun resultToIcon(res: AIResult): ImageVector {
    return when (res) {
        AIResult.OK -> Icons.Outlined.CheckCircle
        AIResult.SUSPICIOUS -> Icons.Outlined.PriorityHigh
        AIResult.UNRECOGNIZED -> Icons.Outlined.QuestionMark
    }
}

fun byteArrayToBitmap(image: ByteArray?): ImageBitmap {
    if (image == null) {
        return BitmapFactory.decodeByteArray(ByteArray(0), 0, 0).asImageBitmap()//todo
    }
    return BitmapFactory.decodeByteArray(image, 0, image.size).asImageBitmap()
}

fun byteArrayToCompressedBitmap(image: ByteArray?): ImageBitmap {
    if (image == null) {
        return BitmapFactory.decodeByteArray(ByteArray(0), 0, 0).asImageBitmap()//todo
    }
    val bitmapImage = BitmapFactory.decodeByteArray(image, 0, image.size)
    val scaled = Bitmap.createScaledBitmap(
        bitmapImage, 512,
        ((bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth())).toInt()), true
    )
    return scaled.asImageBitmap()
}

@RequiresApi(Build.VERSION_CODES.P)
fun getContentData(context: Context, uri: Uri?): ByteArray {
    if (uri == null) {
        return ByteArray(0)
    }
    val bitmap = ImageDecoder.decodeBitmap(ImageDecoder.createSource(context.contentResolver, uri))
    val os = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
    return os.toByteArray()
}