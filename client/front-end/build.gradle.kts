val androidxCoreVersion by extra { "1.13.0" }
val androidxAppCompatVersion by extra { "1.6.1" }
val androidMaterialVersion by extra { "1.11.0" }
val androidxConstraintVersion by extra { "2.1.4" }
val androidxLifecycleVersion by extra { "2.7.0" }
val androidxNavigationVersion by extra { "2.7.7" }
val androidxEspressoVersion by extra { "3.5.1" }
val androidxActivityVersion by extra { "1.9.0" }
val androidxLifecycleRuntimeComposeVersion by extra { "2.7.0" }
val androidxComposeRuntimeVersion by extra { "1.7.0-alpha07" }
val androidxMediaVersion by extra { "1.7.0" }
val androidxCameraxVersion by extra { "1.3.3" }

val retrofitVersion by extra { "2.6.2" }

val auth0AndroidAuth0Version by extra { "2.10.2" }
val auth0AndroidJwtDecodeVersion by extra { "2.0.2" }

val junitVersion by extra { "4.13.2" }
val androidxJunitVersion by extra { "1.1.5" }

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "cz.cvut.fel.thesis"
    compileSdk = 34

    defaultConfig {
        applicationId = "cz.cvut.fel.thesis"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
        manifestPlaceholders["auth0Domain"] = "@string/com_auth0_domain"
        manifestPlaceholders["auth0Scheme"] = "@string/com_auth0_scheme"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    java {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.4"
    }
}

dependencies {
    implementation(project(":logic"))
    implementation("androidx.core:core-ktx:$androidxCoreVersion")
    implementation("androidx.appcompat:appcompat:$androidxAppCompatVersion")
    implementation("com.google.android.material:material:$androidMaterialVersion")
    implementation("androidx.constraintlayout:constraintlayout:$androidxConstraintVersion")

    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$androidxLifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$androidxLifecycleVersion")
    implementation("androidx.lifecycle:lifecycle-runtime-compose:$androidxLifecycleRuntimeComposeVersion")

    implementation("androidx.compose.runtime:runtime:$androidxComposeRuntimeVersion")
    implementation("androidx.compose.runtime:runtime-livedata:$androidxComposeRuntimeVersion")

    implementation("androidx.navigation:navigation-fragment-ktx:$androidxNavigationVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$androidxNavigationVersion")
    implementation("androidx.activity:activity-compose:$androidxActivityVersion")
    implementation("androidx.navigation:navigation-compose:$androidxNavigationVersion")


    implementation(platform("androidx.compose:compose-bom:2024.04.01"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.material:material-icons-extended")

    implementation("androidx.media:media:$androidxMediaVersion")

    implementation("androidx.camera:camera-core:$androidxCameraxVersion")
    implementation("androidx.camera:camera-camera2:$androidxCameraxVersion")
    implementation("androidx.camera:camera-lifecycle:$androidxCameraxVersion")
    implementation("androidx.camera:camera-video:$androidxCameraxVersion")
    implementation("androidx.camera:camera-view:$androidxCameraxVersion")
    implementation("androidx.camera:camera-extensions:$androidxCameraxVersion")

    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")

    implementation("com.auth0.android:auth0:$auth0AndroidAuth0Version")
    implementation("com.auth0.android:jwtdecode:$auth0AndroidJwtDecodeVersion")

    testImplementation("junit:junit:$junitVersion")
    androidTestImplementation("androidx.test.ext:junit:$androidxJunitVersion")
    androidTestImplementation("androidx.test.espresso:espresso-core:$androidxEspressoVersion")
}
