val retrofitVersion by extra { "2.11.0" }
val okhttpLoggingInterceptorVersion by extra { "5.0.0-alpha.12" }
val moshiKotlinVersion by extra { "1.15.1" }
val imgScalrVersion by extra { "4.2" }
val kotlinTestVersion by extra { "2.0.7" }
val kotlinTestRunnerVersion by extra { "3.4.2" }
val mockitoVersion by extra {"5.12.0"}


plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
    id("org.openapi.generator") version "7.4.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}


openApiGenerate {
    generatorName.set("kotlin")
    inputSpec.set("$rootDir/logic/src/main/resources/server.yaml")
    outputDir.set(layout.buildDirectory.dir("/generated").get().toString())
    apiPackage.set("cz.cvut.fel.thesis.logic.generated.client")
    invokerPackage.set("cz.cvut.fel.thesis.logic.generated.invoker")
    modelPackage.set("cz.cvut.fel.thesis.logic.generated.model")
    configOptions.set(mapOf("dateLibrary" to "java8"))
    library.set("jvm-retrofit2")
}
dependencies {
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-scalars:$retrofitVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okhttpLoggingInterceptorVersion")
    implementation("com.squareup.moshi:moshi-kotlin:$moshiKotlinVersion")
    implementation("com.squareup.retrofit2:converter-gson:$retrofitVersion")

    implementation("org.imgscalr:imgscalr-lib:$imgScalrVersion")

    testImplementation("io.kotlintest:kotlintest:$kotlinTestVersion")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:$kotlinTestRunnerVersion")
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
}

tasks.compileKotlin {
    dependsOn(tasks.openApiGenerate)
}


sourceSets {
    main {
        kotlin {
            srcDir(layout.buildDirectory.dir("/generated/src/main/kotlin").get().toString())
        }
    }
    test {
        kotlin {
            srcDir(layout.buildDirectory.dir("/generated/src/test/kotlin").get().toString())
        }
    }
}


