package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.generated.model.RecordDto
import cz.cvut.fel.thesis.logic.generated.model.RecordInputDto
import cz.cvut.fel.thesis.logic.service.RecordService
import cz.cvut.fel.thesis.logic.service.impl.RecordServiceImpl
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.mockito.Mockito.mock
import java.time.OffsetDateTime

class RecordMapperTest {
    private val recordService: RecordService = mock(RecordServiceImpl::class.java)
    private val recordMapper = RecordMapper(recordService = recordService)

    @Test
    fun recordToInputDtoTest() {
        val description = "description"
        val imgId = "imgId"
        val r = PhotoRecord(placement = Placement.FACE, description = description, imgId = imgId)

        val res = recordMapper.recordToInputDto(r)

        assertEquals(description, res.description)
        assertEquals(imgId, res.photoId)
        assertEquals(RecordInputDto.Placement.FACE, res.placement)
    }

    @Test
    fun dtoToRecordNoImageTest() {
        val id = "id"
        val createdOn = OffsetDateTime.now()
        val placement = RecordDto.Placement.FACE
        val imgId = "imgId"
        val description = "description"
        val numberOfRelated = 15
        val r = RecordDto(
            id = id,
            createdOn = createdOn,
            placement = placement,
            imgId = imgId,
            description = description,
            numberOfRelated = numberOfRelated
        )

        val res = recordMapper.dtoToRecord(r, false)

        assertEquals(id, res.id)
        assertEquals(createdOn, res.created)
        assertEquals(Placement.FACE, res.placement)
        assertEquals(imgId, res.imgId)
        assertEquals(description, res.description)
        assertEquals(numberOfRelated, res.relatedNumber)
    }
}