package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import cz.cvut.fel.thesis.logic.generated.model.SpecificationDto
import org.junit.Assert.assertEquals
import org.junit.Test

class SpecificationMapperTest {
    private val specificationMapper = SpecificationMapper()
    @Test
    fun dtoToSpecificationOKTest(){
        val id = "id"
        val aiResult = SpecificationDto.AiResult.OK
        val image = ""
        val dto = SpecificationDto(id = id, aiResult = aiResult, croppedImage = image)

        val specification = specificationMapper.dtoToSpecification(dto = dto)

        assertEquals(id, specification.id)
        assertEquals(AIResult.OK, specification.result)
    }
    @Test
    fun dtoToSpecificationUnrecognizedTest(){
        val id = "id"
        val aiResult = SpecificationDto.AiResult.UNRECOGNIZED
        val image = ""
        val dto = SpecificationDto(id = id, aiResult = aiResult, croppedImage = image)

        val specification = specificationMapper.dtoToSpecification(dto = dto)

        assertEquals(id, specification.id)
        assertEquals(AIResult.UNRECOGNIZED, specification.result)
    }

    @Test
    fun dtoToSpecificationSuspiciousTest(){
        val id = "id"
        val aiResult = SpecificationDto.AiResult.SUSPICIOUS
        val image = ""
        val dto = SpecificationDto(id = id, aiResult = aiResult, croppedImage = image)

        val specification = specificationMapper.dtoToSpecification(dto = dto)

        assertEquals(id, specification.id)
        assertEquals(AIResult.SUSPICIOUS, specification.result)
    }
}