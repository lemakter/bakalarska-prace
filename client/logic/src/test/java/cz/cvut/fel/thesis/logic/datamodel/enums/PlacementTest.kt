package cz.cvut.fel.thesis.logic.datamodel.enums

import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test

class PlacementTest {
    private val m: Mapping = Mapping("")

    @Test
    fun getRecordsForPlacementTest() {
        val placement1 = Placement.FACE
        val placement2 = Placement.LEFT_CALF
        val placement3 = Placement.BUTTOCK
        val r1 = PhotoRecord(placement = placement1, description = "", imgId = "")
        r1.id =""
        val r21 = PhotoRecord(placement = placement2, description = "", imgId = "")
        r21.id = ""
        val r22 = PhotoRecord(placement = placement2, description = "", imgId = "")
        r22.id = ""
        val list = listOf(r1, r21, r22)
        m.addRecords(list)

        //returns all records with correct placement
        val res1 = placement1.recordsFor(m)

        assertEquals(1, res1.size)
        assertTrue(res1.contains(r1))

        //does not return any extra records
        val res2 = placement2.recordsFor(m)

        assertEquals(2, res2.size)
        assertTrue(res2.contains(r21))
        assertTrue(res2.contains(r22))

        //returns emtpy list if no records exist
        val res3 = placement3.recordsFor(m)

        assertTrue(res3.isEmpty())
    }

}