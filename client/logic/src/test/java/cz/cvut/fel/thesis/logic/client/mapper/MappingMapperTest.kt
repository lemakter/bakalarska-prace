package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.generated.model.MappingDto
import cz.cvut.fel.thesis.logic.generated.model.MappingOverviewDto
import cz.cvut.fel.thesis.logic.service.impl.RecordServiceImpl
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.mockito.Mockito.mock
import java.time.OffsetDateTime

class MappingMapperTest {
    private val recordService = mock(RecordServiceImpl::class.java)
    private val mappingMapper = MappingMapper(recordService)

    @Test
    fun overviewToMappingTest() {
        val id = "id"
        val note = "note"
        val date = OffsetDateTime.now()
        val mappingNumber: Long = 13
        val dto = MappingOverviewDto(
            id = id,
            note = note,
            lastUpdated = date,
            firstCreated = date,
            mappingNumer = mappingNumber
        )

        val res = mappingMapper.dtoToMapping(dto)

        assertEquals(id, res.id)
        assertEquals(note, res.note)
        assertEquals(date, res.lastRecordDate())
        assertEquals(date, res.firstRecordDate())
        assertEquals(mappingNumber, res.mappingNumber)
    }

    @Test
    fun mappingToDtoTest() {
        val id = "id"
        val note = "note"
        val mappingNumber: Long = 13
        val dto = MappingDto(
            id = id,
            note = note,
            mappingNumber = mappingNumber)


        val res = mappingMapper.dtoToMapping(dto)

        assertEquals(id, res.id)
        assertEquals(note, res.note)
        assertEquals(mappingNumber, res.mappingNumber)
    }
}