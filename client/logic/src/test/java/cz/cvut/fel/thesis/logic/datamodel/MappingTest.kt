package cz.cvut.fel.thesis.logic.datamodel

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

class MappingTest {

    private val m = Mapping("")

    @Test
    fun okAIResultTest(){
        val r = mock(PhotoRecord::class.java)
        `when`(r.result).thenReturn(AIResult.OK)
        val r1 = mock(PhotoRecord::class.java)
        `when`(r1.result).thenReturn(AIResult.OK)

        m.addRecords(listOf(r, r1))

        val res = m.getAIResult()

        assertEquals(AIResult.OK, res)
    }

    @Test
    fun unrecognisedAIResultTest(){
        val r = mock(PhotoRecord::class.java)
        `when`(r.result).thenReturn(AIResult.OK)
        val r1 = mock(PhotoRecord::class.java)
        `when`(r1.result).thenReturn(AIResult.UNRECOGNIZED)

        m.addRecords(listOf(r, r1))

        val res = m.getAIResult()

        assertEquals(AIResult.UNRECOGNIZED, res)
    }

    @Test
    fun suspiciousAIResultTest(){
        val r = mock(PhotoRecord::class.java)
        `when`(r.result).thenReturn(AIResult.OK)
        val r1 = mock(PhotoRecord::class.java)
        `when`(r1.result).thenReturn(AIResult.SUSPICIOUS)

        m.addRecords(listOf(r, r1))

        val res = m.getAIResult()

        assertEquals(AIResult.SUSPICIOUS, res)
    }

    @Test
    fun suspiciousWithUnrecognisedAIResultTest(){
        val r = mock(PhotoRecord::class.java)
        `when`(r.result).thenReturn(AIResult.UNRECOGNIZED)
        val r1 = mock(PhotoRecord::class.java)
        `when`(r1.result).thenReturn(AIResult.SUSPICIOUS)

        m.addRecords(listOf(r, r1))

        val res = m.getAIResult()

        assertEquals(AIResult.SUSPICIOUS, res)
    }
}