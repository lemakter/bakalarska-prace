package cz.cvut.fel.thesis.logic.service

import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping

/**
 * Service for retrieving Mappings via the MappingController API
 *
 * @author lemakter
 */
interface MappingService {
    /**
     * Make an API call to retrieve overviews (Mapping instances without any related PhotoRecords)
     * for all existing Mappings for the logged in user.
     * @return List of the Mapping overviews
     */
    fun findAllChronological(): List<Mapping>

    /**
     * Make an API call to retrieve the newest Mapping for the logged in user.
     * @return newest Mapping belonging to the user, if no such Mapping exists, return null
     */
    fun currentMapping(): Mapping?

    /**
     * Make an API call to retrieve a Mapping by it's String id.
     * @param id String id of the Mapping to be retrieved
     * @param withDiscontinued Boolean flag marking whether record marked as discontinued
     * should be retrieved.
     * @return Mapping with passed String id, if such Mapping exists and belongs to the
     * logged in user, if not return null
     */
    fun getMapping(id: String, withDiscontinued: Boolean): Mapping?
}