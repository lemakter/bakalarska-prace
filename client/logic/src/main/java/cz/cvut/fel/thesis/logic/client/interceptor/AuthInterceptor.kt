package cz.cvut.fel.thesis.logic.client.interceptor

import okhttp3.Interceptor
import okhttp3.Response


class AuthInterceptor(private val getToken: () -> String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // If token has been saved, add it to the request
        requestBuilder.addHeader("Authorization", "Bearer ${getToken.invoke()}")

        return chain.proceed(requestBuilder.build())
    }
}