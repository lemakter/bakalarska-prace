package cz.cvut.fel.thesis.logic.service

import cz.cvut.fel.thesis.logic.datamodel.AccountSettings

class SettingsServiceDummy: SettingsService {
    private val acc = AccountSettings(username = "dummy", email = "dummy@example.com")
    override fun getUserSettings(): AccountSettings {
        return acc
    }

}