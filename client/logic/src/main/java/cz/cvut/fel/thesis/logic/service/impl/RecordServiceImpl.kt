package cz.cvut.fel.thesis.logic.service.impl

import cz.cvut.fel.thesis.logic.client.mapper.RecordMapper
import cz.cvut.fel.thesis.logic.client.mapper.SpecificationMapper
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.generated.client.PhotoControllerApi
import cz.cvut.fel.thesis.logic.generated.client.RecordControllerApi
import cz.cvut.fel.thesis.logic.generated.model.PairStringRecordInputDto
import cz.cvut.fel.thesis.logic.service.RecordService
import cz.cvut.fel.thesis.logic.utils.imageToString
import cz.cvut.fel.thesis.logic.utils.stringToImage


class RecordServiceImpl(
    private val photoController: PhotoControllerApi,
    private val recordController: RecordControllerApi,
    specificationMapper: SpecificationMapper = SpecificationMapper()
) :
    RecordService {
    private val recordMapper: RecordMapper = RecordMapper(this, specificationMapper)
    override fun reviewRecord(record: PhotoRecord) {
        recordController.addRecord(recordMapper.recordToInputDto(record)).execute()
    }

    override fun batchReview(r: List<Pair<String, PhotoRecord>>) {
        recordController.batchCreateRecords(r.map { a ->
            PairStringRecordInputDto(
                a.first,
                recordMapper.recordToInputDto(a.second)
            )
        })
            .execute()
    }


    override fun evalImage(img: ByteArray): String {
        val res = photoController.createPhoto(imageToString(img)).execute()
        return res.body().orEmpty()
    }

    override fun getImage(id: String): ByteArray {
        val res = photoController.getPhoto(id).execute()
        return stringToImage(res.body().orEmpty())
    }

    override fun discontinue(r: PhotoRecord) {
        recordController.discontinueRecord(r.id).execute()
    }

    override fun rerunAnalysis(r: PhotoRecord) {
        recordController.rerunAnalysis(r.id).execute()
    }

    override fun retrieveRelatedRecords(r: PhotoRecord): List<PhotoRecord> {
        val ret = recordController.retrieveRelated(r.id).execute()
        return ret.body()?.map { rec -> recordMapper.dtoToRecord(rec, true) } ?: listOf()
    }

    override fun newFromExisting(new: PhotoRecord, prev: PhotoRecord) {
        recordController.newRecordFromExisting(prev.id, recordMapper.recordToInputDto(new))
            .execute()
    }

    override fun getResultImage(imgId:String):ByteArray?{
        val ret = photoController.getResultPhoto(imgId).execute()
        if(ret.code()==204){
            return null
        }
        return stringToImage(ret.body().orEmpty())

    }

}