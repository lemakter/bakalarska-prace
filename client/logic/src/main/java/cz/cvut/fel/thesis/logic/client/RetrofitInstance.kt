package cz.cvut.fel.thesis.logic.client

import cz.cvut.fel.thesis.logic.client.interceptor.AuthInterceptor
import cz.cvut.fel.thesis.logic.generated.client.AccountSettingsControllerApi
import cz.cvut.fel.thesis.logic.generated.client.MappingControllerApi
import cz.cvut.fel.thesis.logic.generated.client.PhotoControllerApi
import cz.cvut.fel.thesis.logic.generated.client.RecordControllerApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.openapitools.client.infrastructure.Serializer
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {
    private const val BASE_URL = "http://192.168.0.101:8090/" //Change IP address here
    private val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS)
    private val auth = AuthInterceptor { TokenProvider.getInstance().token }
    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(300, TimeUnit.SECONDS)
        .addInterceptor(logging)
        .addInterceptor(auth)
        .build()

    val photoController: PhotoControllerApi by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(Serializer.moshiBuilder.build()))
            .client(okHttpClient)
            .build()
        retrofit.create(PhotoControllerApi::class.java)
    }
    val mappingController: MappingControllerApi by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(Serializer.moshiBuilder.build()))
            .client(okHttpClient)
            .build()
        retrofit.create(MappingControllerApi::class.java)
    }
    val recordController: RecordControllerApi by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(Serializer.moshiBuilder.build()))
            .client(okHttpClient)
            .build()
        retrofit.create(RecordControllerApi::class.java)
    }
    val accountSettingsController: AccountSettingsControllerApi by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(Serializer.moshiBuilder.build()))
            .client(okHttpClient)
            .build()
        retrofit.create(AccountSettingsControllerApi::class.java)
    }
}