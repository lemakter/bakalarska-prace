package cz.cvut.fel.thesis.logic.service

import cz.cvut.fel.thesis.logic.datamodel.AccountSettings

interface SettingsService {
    fun getUserSettings(): AccountSettings
}