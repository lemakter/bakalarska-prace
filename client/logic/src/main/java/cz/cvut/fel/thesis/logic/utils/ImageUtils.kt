package cz.cvut.fel.thesis.logic.utils


import java.util.Base64

fun imageToString(byteArray: ByteArray): String {
    return Base64.getEncoder().encodeToString(byteArray)
}

fun stringToImage(string: String): ByteArray {
    return Base64.getDecoder().decode(string)
}