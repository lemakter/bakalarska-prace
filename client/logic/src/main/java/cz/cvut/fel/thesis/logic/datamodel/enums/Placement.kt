package cz.cvut.fel.thesis.logic.datamodel.enums

import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord

enum class Placement(val lable: String) {
    CHEST("Chest"),
    STOMACH("Stomach"),
    UPPER_BACK("Upper Back"),
    LOWER_BACK("Lower Back"),
    BUTTOCK("Buttock"),
    RIGHT_UPPER_ARM("Right Upper Arm"),
    RIGHT_LOWER_ARM("Right Lower Arm"),
    RIGHT_HAND("Right Hand"),
    LEFT_UPPER_ARM("Left Upper Arm"),
    LEFT_LOWER_ARM("Left Lower Arm"),
    LEFT_HAND("Left Hand"),
    RIGHT_THIGH_FRONT("Right Thigh - Front"),
    RIGHT_THIGH_BACK("Right Thigh - Back"),
    RIGHT_CALF("Right Calf"),
    RIGHT_SHIN("Right Shin"),
    RIGHT_FOOT("Right Foot"),
    LEFT_THIGH_FRONT("Left Thigh - Front"),
    LEFT_THIGH_BACK("Left Thigh - Back"),
    LEFT_CALF("Left Calf"),
    LEFT_SHIN("Left Shin"),
    LEFT_FOOT("Left Foot"),
    NECK("Neck"),
    FACE("Face"),
    HEAD("Head");

    fun recordsFor(mapping: Mapping): List<PhotoRecord> {
        val records = ArrayList<PhotoRecord>()
        for (r in mapping.getRecords()) {
            if (this == r.placement) {
                records.add(r)
            }
        }
        return records
    }


}