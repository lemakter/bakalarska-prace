package cz.cvut.fel.thesis.logic.service.impl


import cz.cvut.fel.thesis.logic.client.mapper.MappingMapper
import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.generated.client.MappingControllerApi
import cz.cvut.fel.thesis.logic.service.MappingService
import cz.cvut.fel.thesis.logic.service.RecordService
import java.util.logging.Level
import java.util.logging.Logger

class MappingServiceImpl(
    private val mappingController: MappingControllerApi,
    recordService: RecordService
) : MappingService {
    private val logger = Logger.getLogger(this.javaClass.name)

    private val mappingMapper: MappingMapper = MappingMapper(recordService)

    override fun findAllChronological(): List<Mapping> {
        val res = mappingController.findAll().execute()
        val list = res.body().orEmpty()
        return list.map { m -> mappingMapper.dtoToMapping(m) }
    }

    override fun currentMapping(): Mapping? {
        val res = mappingController.getNewest().execute()
        val status = res.code()
        logger.log(Level.INFO, "Request status $status")
        val mappingDto = res.body() ?: return null
        return mappingMapper.dtoToMapping(mappingDto, true)
    }

    override fun getMapping(id: String, withDiscontinued: Boolean): Mapping? {
        val res = mappingController.getById(id, withDiscontinued).execute()
        val mappingDto = res.body() ?: return null
        return mappingMapper.dtoToMapping(mappingDto, true)
    }

}