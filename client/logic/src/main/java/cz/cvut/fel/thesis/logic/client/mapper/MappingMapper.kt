package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.datamodel.mapping.Mapping
import cz.cvut.fel.thesis.logic.generated.model.MappingDto
import cz.cvut.fel.thesis.logic.generated.model.MappingOverviewDto
import cz.cvut.fel.thesis.logic.service.RecordService

class MappingMapper(recordService: RecordService) {
    private val recordMapper: RecordMapper = RecordMapper(recordService)

    fun dtoToMapping(dto: MappingDto, withImg: Boolean = false): Mapping {
        val m =
            Mapping(
                id = dto.id.orEmpty(),
                note = dto.note.orEmpty(),
                mappingNumber = dto.mappingNumber ?: 0
            )
        m.addRecords(dto.records.orEmpty().map { r -> recordMapper.dtoToRecord(r, withImg) })
        return m
    }

    fun dtoToMapping(dto: MappingOverviewDto): Mapping {
        return Mapping(
            id = dto.id.orEmpty(),
            note = dto.note.orEmpty(),
            lastUpdated = dto.lastUpdated,
            firstCreated = dto.firstCreated,
            mappingNumber = dto.mappingNumer ?: 0
        )
    }

}