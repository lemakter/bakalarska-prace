package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoSpecification
import cz.cvut.fel.thesis.logic.generated.model.SpecificationDto
import java.util.Base64
import java.util.logging.Level
import java.util.logging.Logger

class SpecificationMapper {
    private val logger = Logger.getLogger(SpecificationMapper::class.simpleName)
    fun dtoToSpecification(dto: SpecificationDto): PhotoSpecification {
        return PhotoSpecification(
            dto.id.orEmpty(),
            Base64.getDecoder().decode(dto.croppedImage),
            dtoToAIResult(dto.aiResult)
        )
    }

    private fun dtoToAIResult(dto: SpecificationDto.AiResult?): AIResult {
        return when (dto) {
            SpecificationDto.AiResult.OK -> AIResult.OK
            SpecificationDto.AiResult.SUSPICIOUS -> AIResult.SUSPICIOUS
            SpecificationDto.AiResult.UNRECOGNIZED -> AIResult.UNRECOGNIZED
            null -> {
                logger.log(
                    Level.INFO,
                    "This should not happen, but is case it does, we're returning OK"
                )
                AIResult.OK
            }
        }

    }
}