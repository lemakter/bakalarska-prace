package cz.cvut.fel.thesis.logic.client

class TokenProvider {

    companion object {
        @Volatile
        private var instance: TokenProvider? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: TokenProvider().also { instance = it }
            }
    }
    var token: String = ""
}