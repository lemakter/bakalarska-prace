package cz.cvut.fel.thesis.logic.datamodel.enums

enum class AIResult( val verbal: String) {
    OK(verbal = "Nothing suspicious found."),
    SUSPICIOUS(
        verbal = "The algorithm has identifier a suspicious lesion."
    ),
    UNRECOGNIZED(
        verbal = "The algorithm has found an unrecognizable lesion."
    );
}