package cz.cvut.fel.thesis.logic.datamodel.mapping

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import java.time.OffsetDateTime
import java.util.stream.Collectors

class Mapping(
    val id: String,
    var note: String = "",
    private var result: AIResult? = null,
    private var lastUpdated: OffsetDateTime? = null,
    private var firstCreated: OffsetDateTime? = null,
    val mappingNumber: Long = 0
) {
    private val records = ArrayList<PhotoRecord>()


    fun getRecords(): List<PhotoRecord> {
        return ArrayList<PhotoRecord>(records)
    }

    fun addRecords(newRecords: List<PhotoRecord>) {
        records.addAll(newRecords)
    }

    fun firstRecordDate(): OffsetDateTime? {
        if (records.isEmpty()) {
            return firstCreated
        }
        return records.stream().map { r -> r.created }.sorted().collect(Collectors.toList())[0]
    }

    fun lastRecordDate(): OffsetDateTime? {
        if (records.isEmpty()) {
            return lastUpdated
        }
        return records.stream().map { r -> r.created }.sorted().collect(Collectors.toList()).last()
    }

    fun getAIResult(): AIResult {
        var res = result ?: AIResult.OK
        for (s in records) {
            if (s.result == AIResult.SUSPICIOUS) {
                res = s.result
                break
            } else if (s.result == AIResult.UNRECOGNIZED) {
                res = s.result
            }
        }
        return res
    }

    fun nonemptyPlacements(): List<Placement> {
        return records.stream().map { r -> r.placement }.distinct().collect(Collectors.toList())
    }
}