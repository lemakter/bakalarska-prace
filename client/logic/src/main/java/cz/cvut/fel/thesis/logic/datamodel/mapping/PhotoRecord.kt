package cz.cvut.fel.thesis.logic.datamodel.mapping

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult
import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import java.time.OffsetDateTime


data class PhotoRecord(
    var img: ByteArray? = null,
    var responseImg: ByteArray? = null,
    val placement: Placement,
    val description: String,
    val specifications: List<PhotoSpecification> = listOf(),
    val created: OffsetDateTime = OffsetDateTime.now(),
    var relatedNumber: Int = 0,
    val imgId: String
) {
    lateinit var id: String
    var result = getAIResult()
    fun getAIResult(): AIResult {
        var res = AIResult.OK
        for (s in specifications) {
            if (s.result == AIResult.SUSPICIOUS) {
                res = s.result
                break
            } else if (s.result == AIResult.UNRECOGNIZED) {
                res = s.result
            }
        }
        return res
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PhotoRecord

        if (placement != other.placement) return false
        if (description != other.description) return false
        if (id != other.id) return false
        if (created != other.created) return false

        return true
    }

    override fun hashCode(): Int {
        var result = placement.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + created.hashCode()
        return result
    }
}