package cz.cvut.fel.thesis.logic.datamodel.mapping

import cz.cvut.fel.thesis.logic.datamodel.enums.AIResult

class PhotoSpecification(val id:String, val croppedImg:ByteArray? = null, val result: AIResult) {
}