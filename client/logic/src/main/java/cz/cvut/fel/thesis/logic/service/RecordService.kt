package cz.cvut.fel.thesis.logic.service

import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord

/**
 * Service for creating, retrieving, and manipulating mappings
 * via RecordController API and PhotoController API.
 *
 * @author lemakter
 */
interface RecordService {
    /**
     * Create and evaluate new PhotoRecord and add it to the existing current Mapping.
     * @param record new PhotoRecord to be created and evaluated
     */
    fun reviewRecord(record: PhotoRecord)

    /**
     * Check resolution of the image, if insufficient, try increasing the resolution programmatically.
     * Make an API call to check quality of the an image, in case the quality is sufficient,
     * return String id of the image.
     * @param img new image in ByteArray format
     * @return String id of successfully evaluated image or empty string if the evaluation failed
     */
    fun evalImage(img: ByteArray): String

    /**
     * Make an API call to retrieve ByteArray of an image by its String id.
     * @param id String id of the image to be retrieved
     * @return compressed image in ByteArray format, if no image with passed id exists, return null
     */
    fun getImage(id: String): ByteArray?

    /**
     * Make an API call to create a new Mapping with newly created PhotoRecords,
     * that are related to previously existing PhotoRecords.
     * @param r List of Pairs of String id of the related PhotoRecord and new PhotoRecord to be created
     */
    fun batchReview(r: List<Pair<String, PhotoRecord>>)

    /**
     * Make an API call to mark passed PhotoRecord as discontinued.
     * @param r PhotoRecord to be marked as discontinued
     */
    fun discontinue(r: PhotoRecord)

    /**
     * Make an API call to rerun the AI analysis and update the PhotoRecord with related Specifications.
     * @param r PhotoRecord, the AI analysis is to be rerun for
     */
    fun rerunAnalysis(r: PhotoRecord)

    /**
     * Make an API call to retrieve related records for selected PhotoRecord.
     * @param r PhotoRecord whose related records are to be retrieved
     * @return sored List of related PhotoRecords including the selected record
     */
    fun retrieveRelatedRecords(r: PhotoRecord): List<PhotoRecord>

    /**
     * Make an API call to create a new PhotoRecord to be marked as related to a preexisting PhotoRecord.
     * @param new new PhotoRecord to be created
     * @param prev preexisting PhotoRecord that is related to the newly created one
     */
    fun newFromExisting(new: PhotoRecord, prev: PhotoRecord)

    /**
     * Make an API call to retrieve ByteArray of an result image by its String id.
     * @param imgId String id of the image to be retrieved
     * @return compressed image in ByteArray format, if no image with passed id exists, return null
     */
    fun getResultImage(imgId:String): ByteArray?
}