package cz.cvut.fel.thesis.logic.datamodel

import java.time.LocalTime

class AccountSettings(val username: String, val email: String) {
    //in days
    var notificationFrequency: Int = 60
    var notificationTime: LocalTime = LocalTime.NOON
    var notificationByMail: Boolean = true
    var notificationByPhoneNotification: Boolean = true
    var notifyWhenEvaluated: Boolean = true
    private var changed: Boolean = false

    fun switchByMail() {
        notificationByMail = !notificationByMail
    }

    fun switchByNotification() {
        notificationByPhoneNotification = !notificationByPhoneNotification
    }

    fun switchWhenEvaluated() {
        notifyWhenEvaluated = !notifyWhenEvaluated
    }

    fun setChanged(changed: Boolean) {
        this.changed = changed
    }
}