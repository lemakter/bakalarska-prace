package cz.cvut.fel.thesis.logic.client.mapper

import cz.cvut.fel.thesis.logic.datamodel.enums.Placement
import cz.cvut.fel.thesis.logic.datamodel.mapping.PhotoRecord
import cz.cvut.fel.thesis.logic.generated.model.RecordDto
import cz.cvut.fel.thesis.logic.generated.model.RecordInputDto
import cz.cvut.fel.thesis.logic.service.RecordService
import java.time.OffsetDateTime

class RecordMapper(
    private val recordService: RecordService,
    private val specificationMapper: SpecificationMapper = SpecificationMapper()
) {

    fun recordToInputDto(r: PhotoRecord): RecordInputDto {
        return RecordInputDto(
            placement = placementToDto(r.placement),
            description = r.description,
            photoId = r.imgId
        )
    }

    fun dtoToRecord(dto: RecordDto, withImg: Boolean): PhotoRecord {
        if (withImg && dto.imgId != null) {
            val img = recordService.getImage(dto.imgId)
            val resImg = recordService.getResultImage(dto.imgId)
            if (img != null) {
                val r = PhotoRecord(
                    img = img,
                    responseImg = resImg,
                    placement = dtoToPlacement(dto.placement),
                    description = dto.description.orEmpty(),
                    specifications = dto.specifications.orEmpty()
                        .map { s -> specificationMapper.dtoToSpecification(s) },
                    created = dto.createdOn ?: OffsetDateTime.now(),
                    imgId = dto.imgId,
                    relatedNumber = dto.numberOfRelated ?: 0
                )
                r.id = dto.id.orEmpty()
                return r
            }
        }
        val r = PhotoRecord(
            placement = dtoToPlacement(dto.placement),
            description = dto.description.orEmpty(),
            specifications = dto.specifications.orEmpty()
                .map { s -> specificationMapper.dtoToSpecification(s) },
            created = dto.createdOn ?: OffsetDateTime.now(),
            imgId = dto.imgId.orEmpty(),
            relatedNumber = dto.numberOfRelated ?: 0
        )
        r.id = dto.id.orEmpty()
        return r
    }

    private fun dtoToPlacement(p: RecordDto.Placement?): Placement {
        return when (p) {
            RecordDto.Placement.CHEST -> Placement.CHEST
            RecordDto.Placement.STOMACH -> Placement.STOMACH
            RecordDto.Placement.UPPER_BACK -> Placement.UPPER_BACK
            RecordDto.Placement.LOWER_BACK -> Placement.LOWER_BACK
            RecordDto.Placement.BUTTOCK -> Placement.BUTTOCK
            RecordDto.Placement.RIGHT_UPPER_ARM -> Placement.RIGHT_UPPER_ARM
            RecordDto.Placement.RIGHT_LOWER_ARM -> Placement.RIGHT_LOWER_ARM
            RecordDto.Placement.RIGHT_HAND -> Placement.RIGHT_HAND
            RecordDto.Placement.LEFT_UPPER_ARM -> Placement.LEFT_UPPER_ARM
            RecordDto.Placement.LEFT_LOWER_ARM -> Placement.LEFT_LOWER_ARM
            RecordDto.Placement.LEFT_HAND -> Placement.LEFT_HAND
            RecordDto.Placement.RIGHT_THIGH_FRONT -> Placement.RIGHT_THIGH_FRONT
            RecordDto.Placement.RIGHT_THIGH_BACK -> Placement.RIGHT_THIGH_BACK
            RecordDto.Placement.RIGHT_CALF -> Placement.RIGHT_CALF
            RecordDto.Placement.RIGHT_SHIN -> Placement.RIGHT_SHIN
            RecordDto.Placement.RIGHT_FOOT -> Placement.RIGHT_FOOT
            RecordDto.Placement.LEFT_THIGH_FRONT -> Placement.LEFT_THIGH_FRONT
            RecordDto.Placement.LEFT_THIGH_BACK -> Placement.LEFT_THIGH_BACK
            RecordDto.Placement.LEFT_CALF -> Placement.LEFT_CALF
            RecordDto.Placement.LEFT_SHIN -> Placement.LEFT_SHIN
            RecordDto.Placement.LEFT_FOOT -> Placement.LEFT_FOOT
            RecordDto.Placement.NECK -> Placement.NECK
            RecordDto.Placement.FACE -> Placement.FACE
            RecordDto.Placement.HEAD -> Placement.HEAD
            null -> Placement.BUTTOCK // This should never happen, so we check this by assigning a random value.
        }
    }

    private fun placementToDto(p: Placement): RecordInputDto.Placement {
        return when (p) {
            Placement.CHEST -> RecordInputDto.Placement.CHEST
            Placement.STOMACH -> RecordInputDto.Placement.STOMACH
            Placement.UPPER_BACK -> RecordInputDto.Placement.UPPER_BACK
            Placement.LOWER_BACK -> RecordInputDto.Placement.LOWER_BACK
            Placement.BUTTOCK -> RecordInputDto.Placement.BUTTOCK
            Placement.RIGHT_UPPER_ARM -> RecordInputDto.Placement.RIGHT_UPPER_ARM
            Placement.RIGHT_LOWER_ARM -> RecordInputDto.Placement.RIGHT_LOWER_ARM
            Placement.RIGHT_HAND -> RecordInputDto.Placement.RIGHT_HAND
            Placement.LEFT_UPPER_ARM -> RecordInputDto.Placement.LEFT_UPPER_ARM
            Placement.LEFT_LOWER_ARM -> RecordInputDto.Placement.LEFT_LOWER_ARM
            Placement.LEFT_HAND -> RecordInputDto.Placement.LEFT_HAND
            Placement.RIGHT_THIGH_FRONT -> RecordInputDto.Placement.RIGHT_THIGH_FRONT
            Placement.RIGHT_THIGH_BACK -> RecordInputDto.Placement.RIGHT_THIGH_BACK
            Placement.RIGHT_CALF -> RecordInputDto.Placement.RIGHT_CALF
            Placement.RIGHT_SHIN -> RecordInputDto.Placement.RIGHT_SHIN
            Placement.RIGHT_FOOT -> RecordInputDto.Placement.RIGHT_FOOT
            Placement.LEFT_THIGH_FRONT -> RecordInputDto.Placement.LEFT_THIGH_FRONT
            Placement.LEFT_THIGH_BACK -> RecordInputDto.Placement.LEFT_THIGH_BACK
            Placement.LEFT_CALF -> RecordInputDto.Placement.LEFT_CALF
            Placement.LEFT_SHIN -> RecordInputDto.Placement.LEFT_SHIN
            Placement.LEFT_FOOT -> RecordInputDto.Placement.LEFT_FOOT
            Placement.NECK -> RecordInputDto.Placement.NECK
            Placement.FACE -> RecordInputDto.Placement.FACE
            Placement.HEAD -> RecordInputDto.Placement.HEAD
        }
    }
}