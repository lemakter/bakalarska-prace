import piq
import numpy as np
import cv2
import os
import torch
import sys


def image_to_tensor(image):
    """ Convert the image to tensor and normalize it to [0, 1]
    [0,1] is essential, as the program is faster
    Args:
        image (image): image to convert
    Returns:
        image (tensor): image as a tensor
    """
    # # convert the image to tensor
    # image = torch.from_numpy(image).permute(2, 0, 1).float()
    # # normalize the image
    # image = image / 255.0
    # # return the image

    image = image.astype(np.float32) / 255.0
    # transpose the image
    image = image.transpose(2, 0, 1)
    # image to tensor
    image = torch.from_numpy(image).float()

    return image


def check_image_quality(path):
    """ Check the image quality
    Args:
        path (String): image to check the quality
    Returns:
        brisque_bool (bool): whether the image is good or not in terms of brisque score
    """
    brisque_bool = None
    # convert the image to tensor
    image = load_image(path)
    image = image_to_tensor(image)[None, ...]
    # calculate the brisque score
    brisque_index = np.nan
    brisque_index = piq.brisque(image, data_range=1., reduction='none')
    if brisque_index > 60:
        brisque_bool = False
    else:
        brisque_bool = True
    brisque_index = brisque_index.detach().numpy()
    return brisque_bool


## Load image

def check_if_file_exists(file_path):
    """ Check if the file exists
    Args:
        file_path (str): path to the file
    Returns:
        bool: whether the file exists or not
    """
    if os.path.isfile(file_path):
        return True
    else:
        return False


def check_if_is_supported_image(image_path):
    """ Check if the image is supported
    Args:
        image_path (str): path to the image
    Returns:
        bool: whether the image is supported or not
    """
    # get the image extension
    image_extension = image_path.split('.')[-1]
    # check if the extension is supported
    if image_extension in ['jpg', 'jpeg', 'JPG', 'JPEG']:
        return True
    else:
        return False


def load_image(image_path):
    """Check if supported and oad the image and convert it to tensor
    Args:
        image_path (str): path to the image
    Returns:
        image (tensor): image as a tensor
    """
    file_exits = check_if_file_exists(image_path)
    if not file_exits:
        raise FileNotFoundError('File {} does not exist'.format(image_path))

    # is_supported_file_format = check_if_is_supported_image(image_path)
    # if not is_supported_file_format:
    #     raise ValueError('File format {} is not supported'.format(image_path.split('.')[-1]))

    # load the image
    image = cv2.imread(image_path)
    # convert the image to RGB, because cv reads images in BGR format
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # image = Image.open(image_path).convert('RGB')
    # image = ImageOps.exif_transpose(image)

    # return the image
    return image


if __name__ == '__main__':
    print(check_image_quality(sys.argv[1]))
