import argparse 
import pandas as pd
import numpy as np

def convert_xyxy_to_xywh(predictions_csv_path, output_csv_path):
    # read predictions csv
    predictions_df = pd.read_csv(predictions_csv_path, converters={'image_id': int})
    predictions_df = predictions_df.copy()
    # get unique image_ids
    image_ids = predictions_df['image_id'].unique()
    # create empty list to store nms predictions
    xywh_predictions = []
    # loop over image_ids with tqdm
    for image_id in image_ids:
        # get predictions for image_id
        image_predictions = predictions_df[predictions_df['image_id'] == image_id].copy()
        # convert image_predictions to xywh
        image_predictions['bbox_width'] = image_predictions['xmax'] - image_predictions['xmin']
        image_predictions['bbox_height'] = image_predictions['ymax'] - image_predictions['ymin']
        # drop bbox_xmax, bbox_ymax
        image_predictions = image_predictions.drop(columns=['xmax', 'ymax'])
        # rename bbox_xmin to bbox_x, bbox_ymin to bbox_y
        image_predictions = image_predictions.rename(columns={'xmin': 'bbox_x', 'ymin': 'bbox_y'})
        # append image_predictions to xywh_predictions
        xywh_predictions.append(image_predictions)
    # concatenate xywh_predictions
    xywh_predictions = pd.concat(xywh_predictions)
    # save xywh_predictions to csv
    xywh_predictions.to_csv(output_csv_path, index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert predictions from xyxy to xywh format')
    parser.add_argument('-i','--input_csv_path', type=str, help='Path to predictions csv file')
    parser.add_argument('-o','--output_csv_path', type=str, help='Path to output csv file')
    args = parser.parse_args()
    convert_xyxy_to_xywh(args.input_csv_path, args.output_csv_path)
