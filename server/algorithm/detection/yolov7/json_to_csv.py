
#necessary imports
import json
import csv
import sys
import argparse
from tqdm import tqdm




# write function to convert json to csv with column names 
# the json consists of  "image_id": int, "bbox": string([xmin, ymin, w, h]), "score": float, "category_id": int
# each bbox will have it's own column [xmin, ymin, xmax, ymax]
# the bbox string will be converted to a list of 4 floats

def json_to_csv(json_file, csv_file):
    # open json file
    with open(json_file) as f:
        # load json file
        data = json.load(f)
        # open csv file
        with open(csv_file, 'w') as csvfile:
            # create csv writer object
            csvwriter = csv.writer(csvfile)
            # write column names
            csvwriter.writerow(['image_id', 'xmin', 'ymin', 'xmax', 'ymax', 'score', 'category_id'])
            # iterate through each image

            for image in tqdm(data, total = len(data)):
                # get image id
                image_id = int(image['image_id'])
                # get bbox list
                bbox = image['bbox']
                # get score
                score = image['score']
                # get category id
                category_id = image['category_id']
                # get xmin
                xmin = bbox[0]
                # get ymin
                ymin = bbox[1]
                # get xmax
                xmax = xmin + bbox[2]
                # get ymax
                ymax = ymin + bbox[3]
                # write to csv file
                csvwriter.writerow([image_id, xmin, ymin, xmax, ymax, score, category_id])




# # make the function to be callable from the command line
# if __name__ == '__main__':
#     # get json file name from command line
#     json_file = sys.argv[1]
#     # get csv file name from command line
#     csv_file = sys.argv[2]
#     # call function
#     json_to_csv(json_file, csv_file)


# main function with argparse arguments as variables
def main(argparse1, argparse2):
    json_to_csv(argparse1, argparse2)

# make the function to be callable from the command line
if __name__ == '__main__':
    # get json file name from command line
    parser = argparse.ArgumentParser()
    # -j is the short version of --json
    # -c is the short version of --csv
    parser.add_argument('-j', '--json', help='json file name', required=True)
    parser.add_argument('-c', '--csv', help='csv file name', required=True)

    args = parser.parse_args()
    # call function
    main(args.json, args.csv)