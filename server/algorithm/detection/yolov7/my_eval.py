# generate evaluation script for object detection using the torchmetrics.detection.mean_ap.py
# i have 2 folders with ground truth and detection files
# each file has its own name and contains the bounding boxes for each image
# the structure of prediction files is: class_id, score, xmin, ymin, xmax, ymax
# the structure of ground truth files is: class_id, xmin, ymin, xmax, ymax
# image id is in the name of the file
# the user shall provide the path to the ground truth and prediction folders in terminal
# the user shall provide the path to the output folder in terminal

# imports 

import os
import sys
import pandas as pd
import argparse
import torch
import torchmetrics
from torchmetrics.detection.mean_ap import MeanAveragePrecision
from tqdm import tqdm

# #preds (List): A list consisting of dictionaries each containing the key-values (each dictionary corresponds to a single image). Parameters that should be provided per dict

#         boxes: (FloatTensor) of shape (num_boxes, 4) containing num_boxes detection boxes of the format specified in the constructor. By default, this method expects (xmin, ymin, xmax, ymax) in absolute image coordinates.

#         scores: FloatTensor of shape (num_boxes) containing detection scores for the boxes.

#         labels: IntTensor of shape (num_boxes) containing 0-indexed detection classes for the boxes.



# target (List) A list consisting of dictionaries each containing the key-values (each dictionary corresponds to a single image). Parameters that should be provided per dict:

#         boxes: FloatTensor of shape (num_boxes, 4) containing num_boxes ground truth boxes of the format specified in the constructor. By default, this method expects (xmin, ymin, xmax, ymax) in absolute image coordinates.

#         labels: IntTensor of shape (num_boxes) containing 0-indexed ground truth classes for the boxes.



# get the path to the ground truth and prediction folders from terminal
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-gt", type=str, required=True, help="path to ground truth folder")
    parser.add_argument("-pred", type=str, required=True, help="path to prediction folder")
    # parser.add_argument("-out", type=str, required=True, help="path to output folder")
    args = parser.parse_args()
    return args

# get the list of files in the ground truth and prediction folders
def get_files_list(gt_folder, pred_folder):
    gt_files = os.listdir(gt_folder)
    pred_files = os.listdir(pred_folder)
    pred_files.sort()
    gt_files.sort()
    return gt_files, pred_files

# if not file in prediction folder, create an empty file
def create_empty_file(pred_folder, gt_file):
    if gt_file not in os.listdir(pred_folder):
        with open(os.path.join(pred_folder, gt_file), "w") as f:
            f.write("")

# generate empty files for the prediction folder
def generate_empty_files(gt_folder, pred_folder):
    gt_files, pred_files = get_files_list(gt_folder, pred_folder)
    for gt_file in gt_files:
        create_empty_file(pred_folder, gt_file)


# assert check list of files in the prediction folder and gt folder and compare the length and the order of the files
def assert_check_pred_gt(gt_folder, pred_folder):
    gt_files, pred_files = get_files_list(gt_folder, pred_folder)
    assert len(gt_files) == len(pred_files), "The number of files in the prediction folder and the ground truth folder are not equal"
    assert gt_files == pred_files, "The order of the files in the prediction folder and the ground truth folder are not equal"




# # parse empty file in prediction folder or in ground truth folder
# def parse_empty_file(gt_file, pred_file):
#     if gt_file == pred_file:
#         return pd.DataFrame(columns=["class_id", "xmin", "ymin", "xmax", "ymax"])
#     else:
#         return pd.DataFrame(columns=["class_id", "score", "xmin", "ymin", "xmax", "ymax"])


# # create preds and target list containing dictionaries for each image as defined in the torchmetrics.detection.mean_ap
# # map the class_name to 0
# # if empty file in prediction folder, create empty dictionary
# # if empty file in ground truth folder, create empty dictionary
# def create_dicts(gt_folder, pred_folder):
#     preds = []
#     target = []
#     gt_files, pred_files = get_files_list(gt_folder, pred_folder)
#     for gt_file, pred_file in zip(gt_files, pred_files):
#         gt_df = pd.read_csv(os.path.join(gt_folder, gt_file), header=None, names=["class_id", "xmin", "ymin", "xmax", "ymax"])
#         pred_df = pd.read_csv(os.path.join(pred_folder, pred_file), header=None, names=["class_id", "score", "xmin", "ymin", "xmax", "ymax"])
#         gt_df["class_id"] = gt_df["class_id"].map({"class_name": 0})
#         pred_df["class_id"] = pred_df["class_id"].map({"class_name": 0})
#         if gt_df.empty:
#             gt_df = parse_empty_file(gt_file, pred_file)
#         if pred_df.empty:
#             pred_df = parse_empty_file(gt_file, pred_file)
#         # convert df values also to float and then to tensor
#         gt_df = gt_df.astype(float)
#         pred_df = pred_df.astype(float)
#         gt_df = torch.tensor(gt_df.values)
#         pred_df = torch.tensor(pred_df.values)
#         # create dictionary for each image
#         gt_dict = {"boxes": gt_df[:, 1:], "labels": gt_df[:, 0]}
#         pred_dict = {"boxes": pred_df[:, 2:], "scores": pred_df[:, 1], "labels": pred_df[:, 0]}
#         preds.append(pred_dict)
#         target.append(gt_dict)




    # return preds, target




# create preds and target list containing dictionaries for each image as defined in the torchmetrics.detection.mean_ap
# map the class_name to 0
# if empty file in prediction folder, create empty dictionary
# if empty file in ground truth folder, create empty dictionary
# the files are txt files split by space, the column names are not provided
def create_dicts(gt_folder, pred_folder):
    preds = []
    target = []
    gt_files, pred_files = get_files_list(gt_folder, pred_folder)
    # assert check list of files in the prediction folder and gt folder and compare the length and the order of the files
    assert_check_pred_gt(gt_folder, pred_folder)
    for gt_file, pred_file in tqdm(zip(gt_files, pred_files), total=len(gt_files)):
        # if gt is empty, create empty df
        # if pred is empty, create empty df
        
        gt_df = pd.read_csv(os.path.join(gt_folder, gt_file), header=None, names=["class_id", "xmin", "ymin", "xmax", "ymax"], sep=" ")
        pred_df = pd.read_csv(os.path.join(pred_folder, pred_file), header=None, names=["class_id", "score", "xmin", "ymin", "xmax", "ymax"], sep=" ")
        gt_df["class_id"] = gt_df["class_id"].map({"skin_lesion": 0})
        pred_df["class_id"] = pred_df["class_id"].map({"skin_lesion": 0})
        if gt_df.empty:
            gt_df = pd.DataFrame(columns=["class_id", "xmin", "ymin", "xmax", "ymax"])
        if pred_df.empty:
            pred_df = pd.DataFrame(columns=["class_id", "score", "xmin", "ymin", "xmax", "ymax"])
        # convert df values also to float and then to tensor
        gt_df = gt_df.astype(float)
        pred_df = pred_df.astype(float)
        gt_df = torch.tensor(gt_df.values)
        pred_df = torch.tensor(pred_df.values)
        # create dictionary for each image
        gt_dict = {"boxes": gt_df[:, 1:], "labels": gt_df[:, 0]}
        pred_dict = {"boxes": pred_df[:, 2:], "scores": pred_df[:, 1], "labels": pred_df[:, 0]}
        preds.append(pred_dict)
        target.append(gt_dict)
        # print("preds: ", preds)
        # print("target: ", target)
        # input("Press Enter to continue...")
    return preds, target
    




# main function
def main():
    args = get_args()
    gt_folder = args.gt
    pred_folder = args.pred
    # check if the folder are same
    # compare_files(gt_folder, pred_folder)
    # out_folder = args.out
    # generate_empty_files(gt_folder, pred_folder)
    preds, target = create_dicts(gt_folder, pred_folder)
    print("Preds and targets created")
    mean_ap = MeanAveragePrecision(iou_thresholds=[0.25,0.5, 0.75], num_classes=1)
    mean_ap.update(preds, target)
    from pprint import pprint
    pprint(mean_ap.compute())
    



if __name__ == "__main__":
    main()



