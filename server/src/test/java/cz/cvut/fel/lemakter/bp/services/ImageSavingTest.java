package cz.cvut.fel.lemakter.bp.services;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class can be used for testing whether the images are sent and retrieved correctly via the rest endpoint.
 * These tests could have been used before, but are completely irrelevant now, as the used endpoints are now secured with JWT tokens.
 */
@Slf4j
class ImageSavingTest {
    //    @Test
    void saveImage() {
        try (InputStream s = ImageSavingTest.class.getResourceAsStream("/1.jpg")) {
            byte[] image = s.readAllBytes();
            RequestSpecification request = RestAssured.given().baseUri("http://localhost:8090/photo").body(image).contentType(ContentType.JSON);
            Response response = request.post();
            response.getStatusCode();
        } catch (IOException e) {
            log.warn("Ups");
        }
    }

    //    @Test
    void getImage() {
        String id = "661c0debe3e991357eed031e";
        RequestSpecification request = RestAssured.given().baseUri("http://localhost:8090/photo/" + id);
        Response response = request.get();
        try (OutputStream os = new FileOutputStream("testFile.jpg")) {
            os.write(response.getBody().asByteArray());
        } catch (IOException e) {
            log.warn("Ups");
        }
    }
}
