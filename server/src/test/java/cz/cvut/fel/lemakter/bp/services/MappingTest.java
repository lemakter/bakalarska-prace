package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class to test that Mapping.class methods work properly.
 */
@SpringBootTest
class MappingTest {

    @Test
    void lastUpdatedTest(){
        Mapping m = new Mapping();
        PhotoRecord r = new PhotoRecord();
        LocalDateTime ldt = LocalDateTime.of(2000,10, 26, 0, 0, 0, 0 );
        r.setCreatedOn(ldt);//This record has been created a few years back
        PhotoRecord r1 = new PhotoRecord();
        LocalDateTime ldt1 = LocalDateTime.now();
        r1.setCreatedOn(ldt1);
        //this record is created now
        List<PhotoRecord> records = List.of(r, r1);
        m.addRecords(records);

        LocalDateTime last = m.getLastUpdated();

        assertEquals(ldt1, last);
    }

    @Test
    void firstCreatedTest(){
        Mapping m = new Mapping();
        PhotoRecord r = new PhotoRecord();
        LocalDateTime ldt = LocalDateTime.of(2000,10, 26, 0, 0, 0, 0 );
        r.setCreatedOn(ldt);//This record has been created a few years back
        PhotoRecord r1 = new PhotoRecord();
        LocalDateTime ldt1 = LocalDateTime.now();
        r1.setCreatedOn(ldt1);
        //this record is created now
        List<PhotoRecord> records = List.of(r, r1);
        m.addRecords(records);

        LocalDateTime first = m.getFirstCreated();

        assertEquals(ldt, first);
    }

    @Test
    void withoutDiscontinuedTest(){
        Mapping m = new Mapping();
        PhotoRecord r = new PhotoRecord();
        r.setIsDiscontinued(true);
        PhotoRecord r1 = new PhotoRecord();
        List<PhotoRecord> records = List.of(r, r1);
        m.addRecords(records);

        List<PhotoRecord> without = m.getRecords();

        assertFalse(without.contains(r));
        assertTrue(without.contains(r1));
    }

    @Test
    void withDiscontinuedTest(){
        Mapping m = new Mapping();
        PhotoRecord r = new PhotoRecord();
        r.setIsDiscontinued(true);
        PhotoRecord r1 = new PhotoRecord();
        List<PhotoRecord> records = List.of(r, r1);
        m.addRecords(records);

        List<PhotoRecord> without = m.getRecordsWithDiscontinued();

        assertTrue(without.contains(r));
        assertTrue(without.contains(r1));
    }
}
