package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.repositories.MappingRepository;
import cz.cvut.fel.lemakter.bp.repositories.PhotoRepository;
import cz.cvut.fel.lemakter.bp.repositories.RecordRepository;
import cz.cvut.fel.lemakter.bp.services.impls.AuthorizationServiceImpl;
import cz.cvut.fel.lemakter.bp.services.impls.MappingServiceImpl;
import cz.cvut.fel.lemakter.bp.services.impls.PhotoServiceImpl;
import cz.cvut.fel.lemakter.bp.services.impls.RecordServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@DataMongoTest
@Import({MappingServiceImpl.class, PhotoServiceImpl.class, RecordServiceImpl.class})
class AuthorizationServiceTest {
    @Autowired
    private MappingService mappingService;
    @Autowired
    private PhotoService photoService;
    @Autowired
    private RecordService recordService;

    @Autowired
    private MappingRepository mappingRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private RecordRepository recordRepository;

    String username = "test";
    private AuthorizationService authorizationService;

    @BeforeEach
    void setUp() {
        if (authorizationService == null) {
            TokenExporter tokenExporter = mock(TokenExporter.class);
            when(tokenExporter.getUsername(any())).thenReturn(username);
            authorizationService = new AuthorizationServiceImpl(recordService, photoService, mappingService, tokenExporter);
        }
    }

    @Test
    void mappingAuthorizationSuccessfulTest() throws EntityNotFoundException, InvalidIdException {
        String id = mappingService.createNewMapping(username);

        boolean isAuth = authorizationService.authorizeMappingAccess(id);

        assertTrue(isAuth);

        mappingRepository.deleteById(new ObjectId(id));
    }

    @Test
    void mappingAuthorizationFailedTest() throws EntityNotFoundException, InvalidIdException {
        String username1 = "test1";
        String id = mappingService.createNewMapping(username1);

        boolean isAuth = authorizationService.authorizeMappingAccess(id);

        assertFalse(isAuth);

        mappingRepository.deleteById(new ObjectId(id));
    }

    @Test
    void photoAuthorizationSuccessfulTest() throws EntityNotFoundException, InvalidIdException {
        String id = photoService.createNewImage(null, username);

        boolean isAuth = authorizationService.authorizePhotoAccess(id);

        assertTrue(isAuth);

        photoRepository.deleteById(new ObjectId(id));
    }

    @Test
    void photoAuthorizationFailedTest() throws EntityNotFoundException, InvalidIdException {
        String username1 = "test1";
        String id1 = photoService.createNewImage(null, username1);

        boolean isAuth = authorizationService.authorizePhotoAccess(id1);

        assertFalse(isAuth);

        photoRepository.deleteById(new ObjectId(id1));
    }

    @Test
    void recordAuthorizationByPhotoSuccessfulTest() throws EntityNotFoundException, InvalidIdException {
        String id = photoService.createNewImage(null, username);
        PhotoRecord r = new PhotoRecord();
        r.setPhotoId(id);
        r = recordRepository.save(r);

        boolean isAuth = authorizationService.authorizeRecordAccess(r.getId().toString());

        assertTrue(isAuth);

        photoRepository.deleteById(new ObjectId(id));
        recordRepository.delete(r);
    }

    @Test
    void recordAuthorizationByPhotoFailTest() throws EntityNotFoundException, InvalidIdException {
        String username1 = "test1";
        String id = photoService.createNewImage(null, username1);
        PhotoRecord r = new PhotoRecord();
        r.setPhotoId(id);
        r = recordRepository.save(r);

        boolean isAuth = authorizationService.authorizeRecordAccess(r.getId().toString());

        assertFalse(isAuth);

        photoRepository.deleteById(new ObjectId(id));
        recordRepository.delete(r);
    }

    @Test
    void recordAuthorizationByMappingSuccessfulTest() throws EntityNotFoundException, InvalidIdException {
        PhotoRecord r = new PhotoRecord();
        String id = photoService.createNewImage(null, username);
        r.setPhotoId(id); //record without a photo cannot exist
        r = recordRepository.save(r);
        mappingService.createNewMapping(username);//the return value can be ignored here
        Mapping m = mappingService.getNewest(username);
        m.addRecord(r);
        mappingService.save(m);

        boolean isAuth = authorizationService.authorizeRecordAccess(r.getId().toString());

        assertTrue(isAuth);

        photoRepository.deleteById(new ObjectId(id));
        mappingRepository.delete(m);
        recordRepository.delete(r);
    }

    @Test
    void recordAuthorizationByMappingFailTest() throws EntityNotFoundException, InvalidIdException {
       String username1 = "test1";
        PhotoRecord r = new PhotoRecord();
        String id = photoService.createNewImage(null, username1);
        r.setPhotoId(id); //record without a photo cannot exist
        r = recordRepository.save(r);
        mappingService.createNewMapping(username1);//the return value can be ignored here
        Mapping m = mappingService.getNewest(username1);
        m.addRecord(r);
        mappingService.save(m);

        boolean isAuth = authorizationService.authorizeRecordAccess(r.getId().toString());

        assertFalse(isAuth);

        photoRepository.deleteById(new ObjectId(id));
        mappingRepository.delete(m);
        recordRepository.delete(r);
    }

    @Test
    void recordAuthorizationByMappingWrongPhotoSuccessfulTest() throws EntityNotFoundException, InvalidIdException {
        String username1 = "test1";
        PhotoRecord r = new PhotoRecord();
        String id = photoService.createNewImage(null, username1);
        r.setPhotoId(id); //record without a photo cannot exist
        r = recordRepository.save(r);
        mappingService.createNewMapping(username);//the return value can be ignored here
        Mapping m = mappingService.getNewest(username);
        m.addRecord(r);
        mappingService.save(m);

        boolean isAuth = authorizationService.authorizeRecordAccess(r.getId().toString());

        assertTrue(isAuth);

        photoRepository.deleteById(new ObjectId(id));
        mappingRepository.delete(m);
        recordRepository.delete(r);
    }
}
