package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.repositories.MappingRepository;
import cz.cvut.fel.lemakter.bp.repositories.RecordRepository;
import cz.cvut.fel.lemakter.bp.services.impls.MappingServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataMongoTest
@Import(MappingServiceImpl.class)
class MappingServiceTest {
    @Autowired
    private MappingService mappingService;

    @Autowired
    private MappingRepository mappingRepository;
    @Autowired
    private RecordRepository recordRepository;

    @Test
    void newMappingIsNewTest() throws EntityNotFoundException, InvalidIdException {
        String username = "test";
        String mappingId = mappingService.createNewMapping(username);

        Mapping m = mappingService.getById(mappingId);
        Mapping m1 = mappingService.getNewest(username);
        boolean newest = m.isNewest();

        assertEquals(m, m1);
        assertTrue(newest);

        //cleaning up after the test
        mappingRepository.delete(m);
    }

    @Test
    void newMappingIsNotCreatedIfNewestHasNotBeenUsedTest() {
        String username = "test";
        String mappingId = mappingService.createNewMapping(username);

        String mappingId1 = mappingService.createNewMapping(username);


        assertEquals(mappingId, mappingId1);

        //cleaning up after the test
        mappingRepository.deleteById(new ObjectId(mappingId));
    }

    @Test
     void getMappingContainingTest() throws EntityNotFoundException {
        String username = "test";
        String mId = mappingService.createNewMapping(username);
        Mapping m = mappingService.getNewest(username);

        assertEquals(mId, m.getId().toString());

        PhotoRecord r = new PhotoRecord();
        r = recordRepository.save(r);
        m.addRecord(r);
        m = mappingService.save(m);
        Mapping m1 = mappingService.getMappingContaining(r).orElse(null);

        assertEquals(m, m1);

        //cleaning up after the test
        mappingRepository.delete(m);
    }
}
