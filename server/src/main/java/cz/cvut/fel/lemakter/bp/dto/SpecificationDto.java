package cz.cvut.fel.lemakter.bp.dto;

import cz.cvut.fel.lemakter.bp.model.enums.AIResult;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class SpecificationDto {
    private String id;
    private AIResult aiResult;
    private String croppedImage;
}
