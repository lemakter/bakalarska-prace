package cz.cvut.fel.lemakter.bp.dto.mappers;

import cz.cvut.fel.lemakter.bp.dto.MappingDto;
import cz.cvut.fel.lemakter.bp.dto.MappingOverviewDto;
import cz.cvut.fel.lemakter.bp.dto.RecordDto;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MappingMapper {
    private final RecordMapper recordMapper;

    public MappingOverviewDto mappingToOverview(Mapping mapping) {
        MappingOverviewDto dto = new MappingOverviewDto();
        dto.setId(mapping.getId().toString());
        dto.setNote(mapping.getNote());
        dto.setLastUpdated(mapping.getLastUpdated() == null ? null : mapping.getLastUpdated().atOffset(ZoneOffset.UTC));
        dto.setFirstCreated(mapping.getFirstCreated() == null ? null : mapping.getFirstCreated().atOffset(ZoneOffset.UTC));
        dto.setMappingNumer(mapping.getMappingNumber());
        return dto;
    }

    public MappingDto mappingToDto(Mapping mapping, boolean withDiscontinued) throws EntityNotFoundException, InvalidIdException {
        MappingDto dto = new MappingDto();
        dto.setId(mapping.getId().toString());
        dto.setNote(mapping.getNote());
        dto.setMappingNumber(mapping.getMappingNumber());
        List<RecordDto> recordDtos = new ArrayList<>();
        for (PhotoRecord r : (withDiscontinued ? mapping.getRecordsWithDiscontinued() : mapping.getRecords())) {
            recordDtos.add(recordMapper.recordToDto(r));
        }
        if (recordDtos.contains(null)) {
            throw new EntityNotFoundException();
        }
        dto.setRecords(recordDtos);
        return dto;
    }

}
