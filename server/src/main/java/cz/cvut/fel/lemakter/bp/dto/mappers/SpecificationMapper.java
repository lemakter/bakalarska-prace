package cz.cvut.fel.lemakter.bp.dto.mappers;

import cz.cvut.fel.lemakter.bp.dto.SpecificationDto;
import cz.cvut.fel.lemakter.bp.model.Specification;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class SpecificationMapper {

    public SpecificationDto specificationToDto(Specification specification) {
        SpecificationDto dto = new SpecificationDto();
        dto.setId(specification.getId().toString());
        dto.setAiResult(specification.getResult());
        dto.setCroppedImage(Base64.getEncoder().encodeToString(specification.getCroppedImg().getImage()));
        return dto;
    }
}
