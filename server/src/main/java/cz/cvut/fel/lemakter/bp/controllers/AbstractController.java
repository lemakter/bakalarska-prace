package cz.cvut.fel.lemakter.bp.controllers;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@RequiredArgsConstructor
public class AbstractController {
    private final TokenExporter tokenExporter;

    /**
     * Protected function to retrieve username from authentication token.
     *
     * @return username/email of the token owner.
     */
    protected String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return tokenExporter.getUsername(authentication);
    }

}
