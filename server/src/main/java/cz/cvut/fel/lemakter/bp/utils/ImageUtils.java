package cz.cvut.fel.lemakter.bp.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

//https://mkyong.com/java/how-to-convert-bufferedimage-to-byte-in-java/
@UtilityClass
@Slf4j
public class ImageUtils {
    final int TARGET_SIZE = 500;
    //This value seemed like a good compromise between limiting the memory usage of the client app and preserving legibility of the image.
    final int MINIMAL_RESOLUTION = 24000000;
    final float MAX_INCREMENT = 1.5f;

    public byte[] toByteArray(BufferedImage bi) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(bi, "jpg", baos);
            return baos.toByteArray();
        }
    }

    public BufferedImage toBufferedImage(byte[] bytes) throws IOException {
        try (InputStream is = new ByteArrayInputStream(bytes)) {
            return ImageIO.read(is);
        }
    }

    public byte[] compress(byte[] bytes) throws IOException {
        BufferedImage img = toBufferedImage(bytes);
        img = Scalr.resize(img, TARGET_SIZE);
        return toByteArray(img);
    }


    public byte[] resizeImageIfNecessary(byte[] bytes) throws IOException {
        BufferedImage img = toBufferedImage(bytes);
        int resolution = img.getWidth() * img.getHeight();
        if (resolution >= MINIMAL_RESOLUTION) {
            return bytes;
        }
        img = Scalr.resize(img, (int) (img.getWidth() * MAX_INCREMENT), (int) (img.getHeight() * MAX_INCREMENT));
        if ((img.getWidth() * img.getHeight()) < MINIMAL_RESOLUTION) {
            log.info("Image resolution is insufficient to resize.");
            return null; //Sonarlint doesn't seem to like this (which is understandable), but it has a reason.
        }
        return toByteArray(img);
    }
}
