package cz.cvut.fel.lemakter.bp.model.enums;

public enum Placement {
    CHEST,
    STOMACH,
    UPPER_BACK,
    LOWER_BACK,
    BUTTOCK,
    RIGHT_UPPER_ARM,
    RIGHT_LOWER_ARM,
    RIGHT_HAND,
    LEFT_UPPER_ARM,
    LEFT_LOWER_ARM,
    LEFT_HAND,
    RIGHT_THIGH_FRONT,
    RIGHT_THIGH_BACK,
    RIGHT_CALF,
    RIGHT_SHIN,
    RIGHT_FOOT,
    LEFT_THIGH_FRONT,
    LEFT_THIGH_BACK,
    LEFT_CALF,
    LEFT_SHIN,
    LEFT_FOOT,
    NECK,
    FACE,
    HEAD
}
