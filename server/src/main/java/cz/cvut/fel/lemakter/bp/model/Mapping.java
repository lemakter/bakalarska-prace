package cz.cvut.fel.lemakter.bp.model;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Document("mapping")
@EqualsAndHashCode
public class Mapping {
    public Mapping(String username, long mappingNumber) {
        this.username = username;
        this.mappingNumber = mappingNumber;
    }

    @Id
    private ObjectId id = new ObjectId();
    @Setter
    private String note;
    @DBRef(lazy = true)
    private List<PhotoRecord> records = new ArrayList<>();
    @Setter
    private boolean newest = true;
    @NotNull
    private String username;
    private long mappingNumber = 0;

    public List<PhotoRecord> getRecords() {
        return records.stream().filter(r -> !r.getIsDiscontinued()).toList();
    }

    public List<PhotoRecord> getRecordsWithDiscontinued() {
        return new ArrayList<>(records);
    }

    public void addRecord(PhotoRecord r) {
        records.add(r);
    }

    public void addRecords(List<PhotoRecord> r) {
        records.addAll(r);
    }

    public LocalDateTime getLastUpdated() {
        List<PhotoRecord> list = getRecords();
        if (list.isEmpty()) {
            if (records.isEmpty()) {
                return null;
            }
            return records.stream().map(PhotoRecord::getCreatedOn).sorted().toList().get(records.size() - 1);

        }
        return list.stream().map(PhotoRecord::getCreatedOn).sorted().toList().get(list.size() - 1);
    }

    public LocalDateTime getFirstCreated() {
        List<PhotoRecord> list = getRecords();
        if (list.isEmpty()) {
            if (records.isEmpty()) {
                return null;
            }
            return records.stream().map(PhotoRecord::getCreatedOn).sorted().toList().get(0);
        }
        return list.stream().map(PhotoRecord::getCreatedOn).sorted().toList().get(0);
    }
}
