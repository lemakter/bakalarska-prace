package cz.cvut.fel.lemakter.bp.controllers;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.dto.AccountSettingsDto;
import cz.cvut.fel.lemakter.bp.dto.mappers.AccountSettingsMapper;
import cz.cvut.fel.lemakter.bp.services.AccountSettingsService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings")
public class AccountSettingsController extends AbstractController {
    private final AccountSettingsService accountSettingsService;
    private final AccountSettingsMapper accountSettingsMapper;

    @Autowired
    public AccountSettingsController(TokenExporter tokenExporter, AccountSettingsService accountSettingsService, AccountSettingsMapper accountSettingsMapper) {
        super(tokenExporter);
        this.accountSettingsService = accountSettingsService;
        this.accountSettingsMapper = accountSettingsMapper;
    }

    @GetMapping
    public ResponseEntity<AccountSettingsDto> getSettings() {
        String username = getUsername();
        return ResponseEntity.ok(accountSettingsMapper.settingsToDto(accountSettingsService.getByUsername(username).orElseThrow()));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changeSettings(@RequestBody @Valid AccountSettingsDto settingsDto) {
        String username = getUsername();
        accountSettingsService.changeAttribute(accountSettingsMapper.dtoToSettings(settingsDto, username));
        return ResponseEntity.ok().build();
    }
}
