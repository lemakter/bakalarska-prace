package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.PhotoWrapper;

/**
 * Service for retrieving, creating photos and evaluating image quality
 *
 * @author lemakter
 */
public interface PhotoService {
    /**
     * @param id String id of the PhotoWrapper
     * @return byteArray of the selected image
     * @throws EntityNotFoundException no PhotoWrapper with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    byte[] getImageById(String id) throws EntityNotFoundException, InvalidIdException;

    /**
     * @param id String id of the PhotoWrapper
     * @return PhotoWrapper instance with selected id
     * @throws EntityNotFoundException no PhotoWrapper with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    PhotoWrapper getPhotoWrapperById(String id) throws EntityNotFoundException, InvalidIdException;

    /**
     * If compressed image exists in the PhotoWrapper return it, else compress image to reduce memory usage on the targeted device
     *
     * @param id String id of the PhotoWrapper object
     * @return byteArray of compressed image
     * @throws EntityNotFoundException no PhotoWrapper with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    byte[] getCompressedImageById(String id) throws EntityNotFoundException, InvalidIdException;


    /**
     * If result image exists in the PhotoWrapper return it, else return null
     *
     * @param id String id of the PhotoWrapper object
     * @return byteArray of result image
     * @throws EntityNotFoundException no PhotoWrapper with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    byte[] getResultImageById(String id) throws EntityNotFoundException, InvalidIdException;

    /**
     * @param image    byteArray of the image
     * @param username name of the owner of the token
     * @return String id of the newly created PhotoWrapper
     */
    String createNewImage(byte[] image, String username);

    /**
     * If image quality is sufficient, create new PhotoWrapper.
     *
     * @param image    byteArray of the image
     * @param username name of the owner of the token
     * @return String id of the newly created PhotoWrapper, if photo quality if insufficient return **null**
     */
    String createNewImageWithQualityCheck(byte[] image, String username);

}
