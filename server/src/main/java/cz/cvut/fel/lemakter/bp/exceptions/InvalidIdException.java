package cz.cvut.fel.lemakter.bp.exceptions;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class InvalidIdException extends Exception {
    private final String message;
}
