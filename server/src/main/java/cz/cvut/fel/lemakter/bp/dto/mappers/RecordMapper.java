package cz.cvut.fel.lemakter.bp.dto.mappers;

import cz.cvut.fel.lemakter.bp.dto.RecordDto;
import cz.cvut.fel.lemakter.bp.dto.RecordInputDto;
import cz.cvut.fel.lemakter.bp.dto.SpecificationDto;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.model.Specification;
import cz.cvut.fel.lemakter.bp.services.RecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RecordMapper {
    private final SpecificationMapper specificationMapper;
    private final RecordService recordService;

    public RecordDto recordToDto(PhotoRecord r) {
        RecordDto dto = new RecordDto();
        dto.setId(r.getId().toString());
        dto.setCreatedOn(r.getCreatedOn().atOffset(ZoneOffset.UTC));
        dto.setDescription(r.getDescription());
        dto.setImgId(r.getPhotoId());
        dto.setPlacement(r.getPlacement());
        dto.setNumberOfRelated(recordService.getNumberOfRelated(r.getId()));
        List<SpecificationDto> specificationDtos = new ArrayList<>();
        for (Specification s : r.getSpecifications()) {
            specificationDtos.add(specificationMapper.specificationToDto(s));
        }
        dto.setSpecifications(specificationDtos);
        return dto;
    }

    public PhotoRecord inputDtoToRecord(RecordInputDto dto) {
        PhotoRecord r = new PhotoRecord();
        r.setCreatedOn(LocalDateTime.now());
        r.setPlacement(dto.getPlacement());
        r.setPhotoId(dto.getPhotoId());
        r.setDescription(dto.getDescription());
        return r;
    }
}
