package cz.cvut.fel.lemakter.bp.services.impls;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.PhotoWrapper;
import cz.cvut.fel.lemakter.bp.repositories.PhotoRepository;
import cz.cvut.fel.lemakter.bp.services.PhotoService;
import cz.cvut.fel.lemakter.bp.utils.ImageUtils;
import cz.cvut.fel.lemakter.bp.utils.PythonScriptUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class PhotoServiceImpl implements PhotoService {
    private final PhotoRepository photoRepository;

    @Override
    public byte[] getImageById(String id) throws EntityNotFoundException, InvalidIdException {
        return getPhotoWrapperById(id).getImage();
    }

    @Override
    public PhotoWrapper getPhotoWrapperById(String id) throws InvalidIdException, EntityNotFoundException {
        ObjectId oId;
        try {
            oId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            throw new InvalidIdException(id);
        }
        return photoRepository.findById(oId).orElseThrow(EntityNotFoundException::new);

    }

    @Override
    public byte[] getCompressedImageById(String id) throws EntityNotFoundException, InvalidIdException {
        PhotoWrapper wrapper = getPhotoWrapperById(id);
        if (wrapper.getCompressed() == null) {
            log.info("Compressing image with id {}", id);
            try {
                wrapper.setCompressed(ImageUtils.compress(wrapper.getImage()));
                photoRepository.save(wrapper);
            } catch (IOException e) {
                log.error("Error compressing image, returning original image", e);
                return wrapper.getImage();
            }
        }
        return wrapper.getCompressed();
    }

    @Override
    public byte[] getResultImageById(String id) throws EntityNotFoundException, InvalidIdException {
        PhotoWrapper wrapper= getPhotoWrapperById(id);
        return wrapper.getResult();
    }

    @Override
    public String createNewImageWithQualityCheck(byte[] image, String username) {
        try {
            image = ImageUtils.resizeImageIfNecessary(image);
        } catch (IOException e) {
            log.warn("Error resizing image with quality check", e);
            return null;
        }
        if (image == null) {
            return null;
        }
        boolean isEnough = qualityCheck(image);
        if (isEnough) {
            return createNewImage(image, username);
        }
        return null;
    }

    @Override
    public String createNewImage(byte[] image, String username) {
        PhotoWrapper wrapper = photoRepository.save(new PhotoWrapper(image, username));
        return wrapper.getId().toString();
    }

    private boolean qualityCheck(byte[] image) {
        File tmp = new File("tmp.jpg");

        //I did not find a better way to find the absolute path of the project regardless the environment.
        String projectPath = tmp.getAbsolutePath();
        projectPath = projectPath.substring(0, projectPath.length() - 8);

        try (OutputStream os = new FileOutputStream(tmp)) {
            os.write(image);
        } catch (IOException e) {
            log.error("Error while writing the temporary file", e);
            tmp.delete();
            return false;
        }
        String path = tmp.getAbsolutePath();
        Runtime rt = Runtime.getRuntime();
        try {
            Process p = rt.exec(new String[]{"python", projectPath + "/algorithm/cli_app/utils/quality_check.py", path});
            boolean res = PythonScriptUtils.readQualityCheckOutput(p);
            p.waitFor();
            return res;
        } catch (IOException e) {
            log.error("Error executing the quality check.", e);
        } catch (InterruptedException e) {
            log.error("Process was interrupted while waiting for the script to stop running.", e);
        } finally {
            tmp.delete();
        }
        return false;
    }
}
