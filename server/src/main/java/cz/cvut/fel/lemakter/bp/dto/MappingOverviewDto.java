package cz.cvut.fel.lemakter.bp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@Setter
@Getter
public class MappingOverviewDto {
    private String id;
    private String note;
    private OffsetDateTime lastUpdated;
    private OffsetDateTime firstCreated;
    private long mappingNumer;
}
