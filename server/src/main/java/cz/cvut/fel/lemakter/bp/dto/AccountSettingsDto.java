package cz.cvut.fel.lemakter.bp.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AccountSettingsDto {

    private int notificationFrequency;
    private int notificationTime;
    private boolean notificationByPhoneNotification;
    private boolean notificationByEmail;
    private boolean notifyWhenEvaluated;
}
