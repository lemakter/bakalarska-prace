package cz.cvut.fel.lemakter.bp.model.enums;

public enum AIResult {
    OK, SUSPICIOUS, UNRECOGNIZED
}
