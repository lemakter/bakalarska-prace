package cz.cvut.fel.lemakter.bp.model;

import cz.cvut.fel.lemakter.bp.model.enums.Placement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class PhotoRecord {
    @Id
    private ObjectId id = new ObjectId();
    private String photoId;
    private List<Specification> specifications = new ArrayList<>();
    private Placement placement;
    private String description;
    private LocalDateTime createdOn;
    private Boolean isDiscontinued = false;
}
