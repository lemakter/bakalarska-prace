package cz.cvut.fel.lemakter.bp.repositories;

import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RecordRepository extends MongoRepository<PhotoRecord, ObjectId> {
    Optional<PhotoRecord> getPhotoRecordByPhotoId(String photoId);
}
