package cz.cvut.fel.lemakter.bp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@RequiredArgsConstructor
@EnableMongoRepositories
@EnableWebSecurity
@Slf4j
public class BPServerApp implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(BPServerApp.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("StartApplication...");
    }

}
