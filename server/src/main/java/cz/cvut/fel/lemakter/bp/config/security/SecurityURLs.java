package cz.cvut.fel.lemakter.bp.config.security;

import lombok.experimental.UtilityClass;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@UtilityClass
public class SecurityURLs {

    static final RequestMatcher PROTECTED_URLS = new OrRequestMatcher(
            new AntPathRequestMatcher("/mapping/**"),
            new AntPathRequestMatcher("/photo/**"),
            new AntPathRequestMatcher("/settings/**"),
            new AntPathRequestMatcher("/record/**")
    );
    static final RequestMatcher PERMIT_ALL_URLS = new OrRequestMatcher(
            new AntPathRequestMatcher("/actuator/**", "GET"),
            new AntPathRequestMatcher("/v3/api-docs/**", "GET"),
            new AntPathRequestMatcher("/swagger-ui/**"),
            new AntPathRequestMatcher("/swagger-ui.html"),
            new AntPathRequestMatcher("/user")
    );
}
