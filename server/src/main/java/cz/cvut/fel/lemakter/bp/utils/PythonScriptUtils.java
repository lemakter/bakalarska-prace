package cz.cvut.fel.lemakter.bp.utils;

import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.model.PhotoWrapper;
import cz.cvut.fel.lemakter.bp.model.Specification;
import cz.cvut.fel.lemakter.bp.model.enums.AIResult;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
@Slf4j
public class PythonScriptUtils {
    private final String RESULT_FOLDER = "\\results\\";

    /**
     * Read the output of the quality check script and determine whether the quality is sufficient or not.
     *
     * @param p Process running the quality check script
     * @return boolean value of the output of the script
     * @throws IOException error reading script output
     */
    public boolean readQualityCheckOutput(Process p) throws IOException {
        String bool;
        try (
                BufferedReader r = p.inputReader();
                BufferedReader errorReader = p.errorReader()
        ) {
            while ((bool = r.readLine()) == null) {
                String q;
                if ((q = errorReader.readLine()) != null) {
                    StringBuilder sb = new StringBuilder(q);
                    //We're reading and logging python errors here
                    do {
                        q = errorReader.readLine();
                        sb.append(q);
                    } while (q != null);
                    log.error(sb.toString());
                    break;
                }
            }
        }
        return "true".equalsIgnoreCase(bool);
    }

    /**
     * Read the algorithm output file and add Specification/s to the PhotoRecord.
     *
     * @param username    name of the owner of the PhotoRecord
     * @param r           PhotoRecord to be updated
     * @param projectPath absolute path of the project
     * @return updated PhotoRecord
     */
    public PhotoRecord updateRecord(String username, PhotoRecord r, String projectPath, PhotoWrapper wrapper) {
        File output = new File(projectPath + RESULT_FOLDER + "output.csv");
        if (!output.exists()) {
            log.error("Output file does not exist {}.", output.getAbsolutePath());
            return r;
        }
        try (BufferedReader br = new BufferedReader(new FileReader(output))) {
            br.readLine(); //Skip the first line which contains the headers
            String line;
            List<Specification> specifications = new ArrayList<>();

            while ((line = br.readLine()) != null) {
                //Create new record Specification for each line on the output.csv
                Specification spec = lineToSpecification(username, line, projectPath);
                if (spec != null) {
                    specifications.add(spec);
                } else {
                    log.warn("Something went wrong while reading specification from the line {}", line);
                    break;
                }
            }

            r.setSpecifications(specifications);


        } catch (IOException e) {
            log.warn("Error reading output file.", e);
            return r;
        }
        if (!r.getSpecifications().isEmpty()) {
            File result = new File(projectPath + RESULT_FOLDER + "results.jpg");
            if (!result.exists()) {
                log.error("Result image file does not exist {}.", result.getAbsolutePath());
                return r;
            }
            try (FileInputStream fis = new FileInputStream(result)) {
                byte[] resImg = fis.readAllBytes();
                wrapper.setResult(ImageUtils.compress(resImg));
            } catch (IOException e) {
                log.warn("Error reading results file.", e);
            }
        }
        return r;
    }

    /**
     * Read one line of the output.csv file and pars it into Specification if possible.
     *
     * @param username    name of the owner of the PhotoRecord/Mapping
     * @param line        one line from the csv file
     * @param projectPath absolute path of the project
     * @return new Specification if it was possible to create one, else null
     */
    private Specification lineToSpecification(String username, String line, String projectPath) {
        String[] spec = line.split(","); //The output file is a .csv
        if (spec.length != 13) {
            log.error("Line {} is malformed", line);
            return null;
        }
        String imgName = spec[0];
        String unknown = spec[11];
        String result = spec[12];

        AIResult res = "true".equalsIgnoreCase(unknown) ? AIResult.UNRECOGNIZED : stringToResult(result);
        File img = new File(projectPath + RESULT_FOLDER + imgName + ".jpg");
        if (!img.exists()) {
            log.error("Image file does not exist");
            return null;
        }

        byte[] bytes;
        try (FileInputStream is = new FileInputStream(img)) {
            bytes = is.readAllBytes();
        } catch (FileNotFoundException e) {
            log.error("Could not find image file {}. This should not happen because the image file exists.", img.getAbsolutePath());
            return null;
        } catch (IOException e) {
            log.error("Could not read image file {}", img.getAbsolutePath());
            return null;
        }

        PhotoWrapper photo = new PhotoWrapper(bytes, username);

        return new Specification(photo, res);
    }

    /**
     * Assigning AIResult enum values to result lesions.
     *
     * @param res String output value, signifying the result lesion type
     * @return Assigned AIResult value
     */
    private AIResult stringToResult(String res) {
        return switch (res) {
            case "AK", "BCC", "SCC", "MEL", "VASC" -> AIResult.SUSPICIOUS;
            case "DF", "BKL", "NV" -> AIResult.OK;
            default -> throw new IllegalStateException("Unexpected value: " + res);
        };
    }
}
