package cz.cvut.fel.lemakter.bp.services.impls;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.model.PhotoWrapper;
import cz.cvut.fel.lemakter.bp.services.AuthorizationService;
import cz.cvut.fel.lemakter.bp.services.MappingService;
import cz.cvut.fel.lemakter.bp.services.PhotoService;
import cz.cvut.fel.lemakter.bp.services.RecordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("authorizationService")
@RequiredArgsConstructor
@Slf4j
public class AuthorizationServiceImpl implements AuthorizationService {
    private final RecordService recordService;
    private final PhotoService photoService;
    private final MappingService mappingService;
    private final TokenExporter tokenExporter;


    @Override
    public boolean authorizeRecordAccess(String recordId) throws EntityNotFoundException, InvalidIdException {
        PhotoRecord r = recordService.getById(recordId);
        Optional<Mapping> m = mappingService.getMappingContaining(r);
        return m.map(a -> a.getUsername().equals(getUsername())).orElse(authorizePhotoAccess(r.getPhotoId()));
    }

    @Override
    public boolean authorizePhotoAccess(String photoId) throws EntityNotFoundException, InvalidIdException {
        PhotoWrapper photoWrapper = photoService.getPhotoWrapperById(photoId);
        return photoWrapper.getUsername().equals(getUsername());
    }

    @Override
    public boolean authorizeMappingAccess(String mappingId) throws EntityNotFoundException, InvalidIdException {
        Mapping m = mappingService.getById(mappingId);
        return m.getUsername().equals(getUsername());
    }

    public String getUsername() { //Making it package private for testing purposes
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return tokenExporter.getUsername(authentication);
    }
}
