package cz.cvut.fel.lemakter.bp.repositories;

import cz.cvut.fel.lemakter.bp.model.RecordHistory;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface HistoryRepository extends MongoRepository<RecordHistory, String> {
    Optional<RecordHistory> findByCurrent(ObjectId id);

    Optional<RecordHistory> findDistinctByRecordsContaining(ObjectId id);
}
