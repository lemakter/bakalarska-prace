package cz.cvut.fel.lemakter.bp.repositories;

import cz.cvut.fel.lemakter.bp.model.Mapping;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MappingRepository extends MongoRepository<Mapping, ObjectId> {
    Optional<Mapping> getDistinctByNewestIsTrueAndUsername(String username);
    List<Mapping> getAllByUsername(String username);
    Optional<Mapping> getMappingByRecordsContaining(ObjectId records);
}
