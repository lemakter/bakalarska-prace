package cz.cvut.fel.lemakter.bp.dto.mappers;

import cz.cvut.fel.lemakter.bp.dto.AccountSettingsDto;
import cz.cvut.fel.lemakter.bp.model.account.AccountSettings;
import org.springframework.stereotype.Component;

@Component
public class AccountSettingsMapper {
    public AccountSettings dtoToSettings(AccountSettingsDto dto, String username) {
        return AccountSettings.builder().username(username)
                .notificationByEmail(dto.isNotificationByEmail())
                .notificationByPhoneNotification(dto.isNotificationByPhoneNotification())
                .notifyWhenEvaluated(dto.isNotifyWhenEvaluated())
                .notificationTime(dto.getNotificationTime())
                .notificationFrequency(dto.getNotificationFrequency()).build();
    }

    public AccountSettingsDto settingsToDto(AccountSettings accountSettings) {
        return AccountSettingsDto.builder()
                .notificationByEmail(accountSettings.isNotificationByEmail())
                .notificationByPhoneNotification(accountSettings.isNotificationByPhoneNotification())
                .notificationFrequency(accountSettings.getNotificationFrequency())
                .notificationTime(accountSettings.getNotificationTime())
                .notifyWhenEvaluated(accountSettings.isNotifyWhenEvaluated())
                .build();
    }
}
