package cz.cvut.fel.lemakter.bp.dto;

import cz.cvut.fel.lemakter.bp.model.enums.Placement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class RecordDto {
    private String id;
    private OffsetDateTime createdOn;
    private List<SpecificationDto> specifications;
    private Placement placement;
    private String imgId;
    private String description;
    private int numberOfRelated;
}
