package cz.cvut.fel.lemakter.bp.model.account;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@Getter
@Setter
@Document(collection = "accountSetting")
@Builder
@AllArgsConstructor
public class AccountSettings {
    @Id
    private String username;
    private int notificationFrequency = 60;
    private int notificationTime = 1080;
    private boolean notificationByPhoneNotification = true;
    private boolean notificationByEmail = false;
    private boolean notifyWhenEvaluated = true;

    public AccountSettings(String username) {
        this.username = username;
    }

}
