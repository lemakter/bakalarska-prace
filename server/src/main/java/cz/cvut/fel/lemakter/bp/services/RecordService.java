package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.exceptions.EntityAlreadyExistsException;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import org.bson.types.ObjectId;
import org.springframework.data.util.Pair;

import java.util.List;

/**
 * Service for retrieving, creating and evaluating PhotoRecords.
 *
 * @author lemakter
 */
public interface RecordService {
    /**
     * Create a new mapping  marked as 'newest' from an existing one, while providing
     * each record with an id of the related record from previous mapping.
     *
     * @param r        List of Pairs containing String id of the previous related record and definition of
     *                 the new record to be created and evaluated
     * @param username name of the owner of the token
     * @return list of String ids of the newly created records
     * @throws EntityNotFoundException      at least on the of the related records' ids do not belong to any PhotoRecord
     * @throws InvalidIdException           conversion on at least one of the related records' ids from String id to ObjectId was unsuccessful
     * @throws EntityAlreadyExistsException at least one record with selected image already exists
     */
    List<String> batchCreate(List<Pair<String, PhotoRecord>> r, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException;

    /**
     * @param id String id of the selected PhotoRecord
     * @return selected PhotoRecord entity
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     * @throws EntityNotFoundException no PhotoRecord with passed id exists
     */
    PhotoRecord getById(String id) throws InvalidIdException, EntityNotFoundException;

    /**
     * Create a new record based on a previously existing record.
     *
     * @param newRecord  new record to be evaluated and created
     * @param previousId String id of the previous
     * @param username   name of the owner of the token
     * @return newly created PhotoRecord
     * @throws EntityNotFoundException      no PhotoRecord with id previousId exists
     * @throws InvalidIdException           conversion of previousId from String id to ObjectId was unsuccessful
     * @throws EntityAlreadyExistsException PhotoRecord with this imageId already exists
     */
    PhotoRecord createFromExisting(PhotoRecord newRecord, String previousId, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException;

    /**
     * Either create new entity, or update existing entity in the database.
     *
     * @param r new or existing PhotoRecord to be created/updated
     * @return created/updated PhotoRecord
     */
    PhotoRecord save(PhotoRecord r);

    /**
     * Run python script to evaluate the related image and update the PhotoRecord with Specification(s).
     *
     * @param r        PhotoRecord to be evaluated and updated
     * @param username name of the owner of the PhotoRecord/Mapping
     * @return updated and evaluated PhotoRecord
     */
    PhotoRecord evaluate(PhotoRecord r, String username);

    /**
     * Create evaluate and save new PhotoRecord and add it into the Mapping marked as 'newest'.
     * If no such mapping exists, create a new Mapping marked as 'newest'.
     *
     * @param r        PhotoRecord to be added saved and added into the Mapping
     * @param username name of the owner of the token
     * @return updated PhotoRecord
     * @throws EntityNotFoundException      this error should not be thrown here and in this case signifies an issue with the database connection
     * @throws InvalidIdException           this error should not be thrown here and in this case signifies an issue with the database
     * @throws EntityAlreadyExistsException conversion from String id to ObjectId was unsuccessful
     */
    PhotoRecord addNew(PhotoRecord r, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException;

    /**
     * Mark selected PhotoRecord as discontinued.
     *
     * @param id String id of the PhotoRecord to be marked as discontinued
     * @throws EntityNotFoundException no PhotoRecord with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    void discontinue(String id) throws EntityNotFoundException, InvalidIdException;

    /**
     * Get number of records present in the PhotoHistory collection, containing passed id.
     *
     * @param recordId ObjectId of the PhotoRecord to be searched for in the PhotoHistory
     * @return number of related records
     */
    int getNumberOfRelated(ObjectId recordId);

    /**
     * Get record present in the PhotoHistory collection containing passed id
     *
     * @param id String id of the PhotoRecord to be searched for in the PhotoHistory
     * @return list of related records including the record with the passed id
     * @throws EntityNotFoundException no PhotoRecord with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    List<PhotoRecord> getRelated(String id) throws EntityNotFoundException, InvalidIdException;
}
