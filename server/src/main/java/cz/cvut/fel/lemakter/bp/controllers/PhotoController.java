package cz.cvut.fel.lemakter.bp.controllers;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.services.PhotoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@RestController
@RequestMapping("/photo")
@Slf4j
public class PhotoController extends AbstractController {
    private final PhotoService photoService;

    @Autowired
    public PhotoController(TokenExporter tokenExporter, PhotoService photoService) {
        super(tokenExporter);
        this.photoService = photoService;
    }

    @Operation(summary = "Retrieve Base64 encoded String of the compressed photo by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "422",
                    description = "Invalid or malformed photo id.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class)))})
    @GetMapping(value = "/{id}")
    @PreAuthorize(value = "@authorizationService.authorizePhotoAccess(#id)")
    public ResponseEntity<String> getPhoto(@PathVariable String id) throws EntityNotFoundException, InvalidIdException {
        String str = Base64.getEncoder().encodeToString(photoService.getCompressedImageById(id));
        return ResponseEntity.ok(str);
    }

    @Operation(summary = "Retrieve Base64 encoded String of the compressed result photo by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "204", description = "The result image does not exist for said image."),
            @ApiResponse(responseCode = "422",
                    description = "Invalid or malformed photo id.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class)))})

    @GetMapping(value = "/{id}/result")
    @PreAuthorize(value = "@authorizationService.authorizePhotoAccess(#id)")
    public ResponseEntity<String> getResultPhoto(@PathVariable String id) throws EntityNotFoundException, InvalidIdException {
        byte[] resImg = photoService.getResultImageById(id);
        if (resImg == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(Base64.getEncoder().encodeToString(resImg));
    }

    @Operation(summary = "Evaluate quality of a photo in format of Base64 encoded String. " +
            "If the quality is sufficient, save image and return String id. If quality is insufficient, return 'null'.")
    @ApiResponse(responseCode = "200",
            description = "OK",
            content = @Content(schema = @Schema(implementation = String.class)))
    @PostMapping
    public ResponseEntity<String> createPhoto(@RequestBody @Valid String image) {
        byte[] bytes = Base64.getDecoder().decode(image);
        return ResponseEntity.ok(photoService.createNewImageWithQualityCheck(bytes, getUsername()));
    }

}
