package cz.cvut.fel.lemakter.bp.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@NoArgsConstructor
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PhotoWrapper {
    public PhotoWrapper(byte[] image, String username) {
        this.image = image;
        this.username = username;
    }

    @Id
    @EqualsAndHashCode.Include
    private ObjectId id = new ObjectId();
    private byte[] image;
    @EqualsAndHashCode.Include
    private String username;
    @Setter
    private byte[] compressed;
    @Setter
    private byte[] result;
}
