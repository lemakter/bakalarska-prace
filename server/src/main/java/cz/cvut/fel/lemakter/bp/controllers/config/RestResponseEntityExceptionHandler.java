package cz.cvut.fel.lemakter.bp.controllers.config;

import cz.cvut.fel.lemakter.bp.exceptions.EntityAlreadyExistsException;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(EntityNotFoundException ex) {
        return ResponseEntity.unprocessableEntity().body(ex);
    }

    @ExceptionHandler(value = {InvalidIdException.class})
    protected ResponseEntity<Object> handleFieldInvalidException(InvalidIdException ex) {
        return ResponseEntity.unprocessableEntity().body(ex);
    }

    @ExceptionHandler(value = {EntityAlreadyExistsException.class})
    protected ResponseEntity<Object> handleEntityAlreadyExistsException(EntityAlreadyExistsException ex) {
        return ResponseEntity.badRequest().build();
    }
}
