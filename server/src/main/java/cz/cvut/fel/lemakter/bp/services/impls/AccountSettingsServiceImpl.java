package cz.cvut.fel.lemakter.bp.services.impls;

import cz.cvut.fel.lemakter.bp.model.account.AccountSettings;
import cz.cvut.fel.lemakter.bp.repositories.AccountSettingsRepository;
import cz.cvut.fel.lemakter.bp.services.AccountSettingsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountSettingsServiceImpl implements AccountSettingsService {
    private final AccountSettingsRepository accountSettingsRepository;

    @Override
    public Optional<AccountSettings> getByUsername(String username) {
        return accountSettingsRepository.findById(username);
    }

    @Override
    public AccountSettings createDefaultSettings(String username) {
        return accountSettingsRepository.save(new AccountSettings(username));
    }

    @Override
    public void changeAttribute(AccountSettings accountSettings) {
        accountSettingsRepository.save(accountSettings);
    }
}
