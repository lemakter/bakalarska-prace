package cz.cvut.fel.lemakter.bp.controllers;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.dto.RecordDto;
import cz.cvut.fel.lemakter.bp.dto.RecordInputDto;
import cz.cvut.fel.lemakter.bp.dto.mappers.RecordMapper;
import cz.cvut.fel.lemakter.bp.exceptions.EntityAlreadyExistsException;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.services.MappingService;
import cz.cvut.fel.lemakter.bp.services.RecordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/record")
@Slf4j
public class RecordController extends AbstractController {
    private final RecordService recordService;
    private final RecordMapper recordMapper;
    private final MappingService mappingService;

    public RecordController(TokenExporter tokenExporter, RecordService recordService, RecordMapper recordMapper, MappingService mappingService) {
        super(tokenExporter);
        this.recordService = recordService;
        this.recordMapper = recordMapper;
        this.mappingService = mappingService;
    }

    @Operation(summary = "Add new record with existing image and add to the mapping marked as 'newest' and return String id of the new mapping. " +
            "If no such mapping exists, creates new mapping marked as 'newest'.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "OK",
                    content = @Content(contentSchema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "422",
                    description = "Invalid image id.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class))),
            @ApiResponse(responseCode = "400", description = "Trying to create a record that already exists.")})
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addRecord(@RequestBody @Valid RecordInputDto r)
            throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        String id = recordService.addNew(recordMapper.inputDtoToRecord(r), getUsername()).getId().toString();
        return ResponseEntity.ok(id);
    }

    @Operation(summary = "If record belong to a mapping of the owner of the token mark record as discontinued.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "422", description = "Incorrect or malformed id.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class))),
            @ApiResponse(responseCode = "403", description = "Record does not belong to the owner of the token.")})
    @PutMapping("/{recordId}")
    @PreAuthorize(value = "@authorizationService.authorizeRecordAccess(#recordId)")
    public void discontinueRecord(@PathVariable("recordId") String recordId) throws EntityNotFoundException, InvalidIdException {
        if (mappingService.getMappingContaining(recordService.getById(recordId))
                .map(m -> !m.getUsername().equals(getUsername())).orElse(false)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        recordService.discontinue(recordId);
    }

    @Operation(summary = "Create a new single record related to an existing record. On success return new record Id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(contentSchema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "422", description = "Related record id is incorrect or malformed.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class))),
            @ApiResponse(responseCode = "400", description = "Trying to create a record that already exists."),
            @ApiResponse(responseCode = "403", description = "Related record does not belong to the owner of the token.")})
    @PostMapping(value = "/{recordId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value = "@authorizationService.authorizeRecordAccess(#recordId)")
    public ResponseEntity<String> newRecordFromExisting(@PathVariable String recordId, @RequestBody @Valid RecordInputDto r) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        String id = recordService.createFromExisting(recordMapper.inputDtoToRecord(r), recordId, getUsername()).getId().toString();
        return ResponseEntity.ok(id);
    }

    @Operation(summary = "Create a new mapping from an existing one, while providing each record with an id " +
            "of the related record from  previous mapping.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "422", description = "One or more related record Ids are incorrect or malformed."
                    , content = @Content(contentSchema = @Schema(implementation = Exception.class))),
            @ApiResponse(responseCode = "400", description = "One or more of the records already exist.")})
    @PostMapping(value = "/batch", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> batchCreateRecords(@RequestBody @Valid List<Pair<String, RecordInputDto>> records) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        String username = getUsername();
        recordService.batchCreate(records.stream().map(r -> Pair.of(r.getFirst(), recordMapper.inputDtoToRecord(r.getSecond()))).toList(), username);
        return ResponseEntity.ok(mappingService.getNewest(username).getId().toString());
    }

    @Operation(summary = "Rerun AI analysis and update specifications of selected record.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "422", description = "Record id is incorrect or malformed."),
            @ApiResponse(responseCode = "403", description = "Selected record does not belong to the owner of the token.")})
    @PutMapping("/{recordId}/rerun")
    @PreAuthorize(value = "@authorizationService.authorizeRecordAccess(#recordId)")
    public void rerunAnalysis(@PathVariable String recordId) throws EntityNotFoundException, InvalidIdException {
        String username = getUsername();
        if (mappingService.getMappingContaining(recordService.getById(recordId))
                .map(m -> !m.getUsername().equals(username)).orElse(false)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        recordService.evaluate(recordService.getById(recordId), username);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = RecordDto.class)))),
            @ApiResponse(responseCode = "422", description = "Record id is incorrect or malformed."),
            @ApiResponse(responseCode = "403", description = "Selected record does not belong to the owner of the token.")})
    @GetMapping("/{recordId}/related")
    @PreAuthorize(value = "@authorizationService.authorizeRecordAccess(#recordId)")
    public ResponseEntity<List<RecordDto>> retrieveRelated(@PathVariable String recordId) throws EntityNotFoundException, InvalidIdException {
        List<PhotoRecord> related = recordService.getRelated(recordId);
        return ResponseEntity.ok(related.stream().map(recordMapper::recordToDto).toList());
    }
}
