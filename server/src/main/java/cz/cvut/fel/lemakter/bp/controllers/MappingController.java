package cz.cvut.fel.lemakter.bp.controllers;

import cz.cvut.fel.lemakter.bp.config.security.TokenExporter;
import cz.cvut.fel.lemakter.bp.dto.MappingDto;
import cz.cvut.fel.lemakter.bp.dto.MappingOverviewDto;
import cz.cvut.fel.lemakter.bp.dto.mappers.MappingMapper;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.services.MappingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(value = "/mapping")
@Slf4j
public class MappingController extends AbstractController {
    //Mappers
    private final MappingMapper mappingMapper;

    //Services
    private final MappingService mappingService;

    @Autowired
    public MappingController(TokenExporter tokenExporter, MappingService mappingService, MappingMapper mappingMapper) {
        super(tokenExporter);
        this.mappingService = mappingService;
        this.mappingMapper = mappingMapper;
    }

    @Operation(summary = "Get overview of all mappings belonging to the owner of the token")
    @ApiResponse(responseCode = "200", description = "OK", content = {@Content(
            mediaType = MediaType.APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = MappingOverviewDto.class)))})

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MappingOverviewDto>> findAll() {
        String username = getUsername();
        return ResponseEntity.ok().body(mappingService.findAll(username).stream().map(mappingMapper::mappingToOverview).toList());
    }


    @Operation(summary = "Retrieve mapping by id, only mappings belonging to the owner of the token can be retrieved")
    @ApiResponses(value =
            {@ApiResponse(
                    responseCode = "200",
                    description = "OK",
                    content = {@Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = MappingDto.class))}),
                    @ApiResponse(
                            responseCode = "403",
                            description = "Mapping owner differs from the owner of the token"),
                    @ApiResponse(
                            responseCode = "422",
                            description = "Mapping with said id does not exist or is malformed.",
                            content = @Content(schema = @Schema(implementation = Exception.class)))
            })
    @GetMapping(value = "/{mappingId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value = "@authorizationService.authorizeMappingAccess(#mappingId)")
    public ResponseEntity<MappingDto> getById(@PathVariable String mappingId, @RequestParam boolean withDiscontinued) throws EntityNotFoundException, InvalidIdException {
        Mapping m = mappingService.getById(mappingId);
        String username = getUsername();
        if (!m.getUsername().equals(username)) {
            log.warn("User {} tried to access mapping belonging to user {}, which is forbidden.", username, m.getUsername());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok().body(mappingMapper.mappingToDto(m, withDiscontinued));
    }


    @Operation(summary = "Retrieve mapping marked as 'newest' belonging to the owner of the token. If no such mapping is found, null is returned.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "OK",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = MappingDto.class))),
            @ApiResponse(
                    responseCode = "422",
                    description = "Selected mapping is malformed.",
                    content = @Content(schema = @Schema(implementation = Exception.class)))
    })
    @GetMapping(value = "/newest")
    public ResponseEntity<MappingDto> getNewest() throws InvalidIdException {
        String username = getUsername();
        try {
            MappingDto m = mappingMapper.mappingToDto(mappingService.getNewest(username), false);
            return ResponseEntity.ok().body(m);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.ok().body(null);
        }
    }

    @Operation(summary = "Created new mapping marked as 'newest' for the owner of the token, and return String id of the new mapping." +
            " If mapping marked as 'newest' already exists, unmark it first.")
    @ApiResponse(responseCode = "200",
            description = "OK",
            content = @Content(contentSchema = @Schema(implementation = String.class)))
    @PostMapping(value = "/new")
    public ResponseEntity<String> createNewMapping() {
        String username = getUsername();
        String id = mappingService.createNewMapping(username);
        return ResponseEntity.ok(id);
    }

    @Operation(summary = "Update the note of mapping by String id. On success return String id of said mapping.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "OK",
                    content = @Content(contentSchema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "422",
                    description = "Incorrect or malformed id.",
                    content = @Content(contentSchema = @Schema(implementation = Exception.class)))
    })
    @PutMapping(value = "/{mappingId}/note")
    @PreAuthorize(value = "@authorizationService.authorizeMappingAccess(#mappingId)")
    public ResponseEntity<String> updateNote(@PathVariable String mappingId, @RequestParam String note) throws EntityNotFoundException, InvalidIdException {
        Mapping mapping = mappingService.getById(mappingId);
        mapping.setNote(note);
        mappingService.save(mapping);
        return ResponseEntity.ok().build();

    }

}
