package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;

/**
 * Service to provide authorization for GET requests, so the data cannout be accessed by wrong users.
 *
 * @author lemakter
 */
public interface AuthorizationService {
    /**
     * Make sure the owner of the token is also the owner of the mapping/image regarding selected PhotoRecord.
     *
     * @param recordId String id of the selected PhotoRecord
     * @return Boolean whether the owner of the token can gain access to the selected PhotoRecord
     * @throws EntityNotFoundException no PhotoRecord with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    boolean authorizeRecordAccess(String recordId) throws EntityNotFoundException, InvalidIdException;

    /**
     * Make sure the owner of the token is also the owner of the PhotoWrapper.
     *
     * @param photoId String id of the selected PhotoWrapper
     * @return Boolean whether the owner of the token can gain access to the selected image
     * @throws EntityNotFoundException no PhotoWrapper with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    boolean authorizePhotoAccess(String photoId) throws EntityNotFoundException, InvalidIdException;

    /**
     * Make sure the owner of the token is also the owner of the Mapping.
     *
     * @param mappingId String id of the selected Mapping
     * @return Boolean whether the owner of the token can gain access to the selected Mapping
     * @throws EntityNotFoundException no Mapping with passed id exists
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    boolean authorizeMappingAccess(String mappingId) throws EntityNotFoundException, InvalidIdException;

}
