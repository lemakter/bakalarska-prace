package cz.cvut.fel.lemakter.bp.model;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Setter
public class RecordHistory {
    @Id
    private ObjectId id = new ObjectId();
    private ObjectId current;
    @DBRef
    private List<PhotoRecord> records = new ArrayList<>();

    public RecordHistory(ObjectId current) {
        this.current = current;
    }

    public int getRelatedCount() {
        return records.size();
    }

    public void addRecord(PhotoRecord r) {
        if (!records.contains(r)) {
            records.add(r);
        }
        records = records.stream().distinct().toList(); // This is probably a bit redundant, and can be removed late
    }

    public List<PhotoRecord> getRecords() {
        return new ArrayList<>(records);
    }
}
