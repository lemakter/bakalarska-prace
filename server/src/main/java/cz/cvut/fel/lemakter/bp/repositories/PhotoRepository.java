package cz.cvut.fel.lemakter.bp.repositories;

import cz.cvut.fel.lemakter.bp.model.PhotoWrapper;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepository extends MongoRepository<PhotoWrapper, ObjectId> {

}
