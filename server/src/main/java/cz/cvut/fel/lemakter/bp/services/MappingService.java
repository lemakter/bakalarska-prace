package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;

import java.util.List;
import java.util.Optional;

/**
 * Service for retrieving and manipulating mappings.
 *
 * @author lemakter
 */
public interface MappingService {
    /**
     * Find all Mappings for a certain user.
     *
     * @param username name of the owner of the token
     * @return list of all Mappings
     */
    List<Mapping> findAll(String username);

    /**
     * @param id String value of ObjectId
     * @return Mapping with passed id
     * @throws EntityNotFoundException Mapping with passed id does not exist
     * @throws InvalidIdException      conversion from String id to ObjectId was unsuccessful
     */
    Mapping getById(String id) throws EntityNotFoundException, InvalidIdException;

    /**
     * Create new Mapping marked as 'newest'. If other Mapping marked as 'newest' exists for the user, unmark it first.
     *
     * @param username name of the owner of the Mapping
     * @return String is of the new Mapping
     */
    String createNewMapping(String username);

    /**
     * Either create new entity, or update existing entity in the database.
     *
     * @param mapping new or existing Mapping to be created/updated
     * @return created/updated Mapping
     */
    Mapping save(Mapping mapping);

    /**
     * Find a Mapping marked as 'newest' for the user.
     *
     * @param username name of the owner of the token
     * @return Mapping marked as 'newest' belonging to the user
     * @throws EntityNotFoundException no Mapping marked as 'newest' exists (this usually means no Mapping exists for the user)
     */
    Mapping getNewest(String username) throws EntityNotFoundException;

    /**
     * Find a Mapping that contains passed PhotoRecord if such Mapping exists
     *
     * @param r selected PhotoRecord
     * @return if Mapping exists, containing selected PhotoRecord, return Optional.Empty, else return the Mapping
     */
    Optional<Mapping> getMappingContaining(PhotoRecord r);
}
