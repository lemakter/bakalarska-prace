package cz.cvut.fel.lemakter.bp.services;

import cz.cvut.fel.lemakter.bp.model.account.AccountSettings;

import java.util.Optional;

public interface AccountSettingsService {
    Optional<AccountSettings> getByUsername(String username);
    AccountSettings createDefaultSettings(String username);
    void changeAttribute(AccountSettings accountSettings);
}
