package cz.cvut.fel.lemakter.bp.services.impls;

import cz.cvut.fel.lemakter.bp.exceptions.EntityAlreadyExistsException;
import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.*;
import cz.cvut.fel.lemakter.bp.repositories.HistoryRepository;
import cz.cvut.fel.lemakter.bp.repositories.PhotoRepository;
import cz.cvut.fel.lemakter.bp.repositories.RecordRepository;
import cz.cvut.fel.lemakter.bp.services.MappingService;
import cz.cvut.fel.lemakter.bp.services.PhotoService;
import cz.cvut.fel.lemakter.bp.services.RecordService;
import cz.cvut.fel.lemakter.bp.utils.PythonScriptUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RecordServiceImpl implements RecordService {
    private final RecordRepository recordRepository;
    private final PhotoService photoService;
    private final MappingService mappingService;
    private final HistoryRepository historyRepository;
    private final PhotoRepository photoRepository;

    @Override
    public List<String> batchCreate(List<Pair<String, PhotoRecord>> r, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        mappingService.createNewMapping(username);
        List<PhotoRecord> list = new ArrayList<>();
        for (Pair<String, PhotoRecord> p : r) {
            PhotoRecord photoRecord = p.getSecond();
            if (recordRepository.getPhotoRecordByPhotoId(photoRecord.getPhotoId()).isPresent()) {
                throw new EntityAlreadyExistsException();
            }
            list.add(createFromExisting(p.getSecond(), p.getFirst(), username));
        }
        return list.stream().map(a -> a.getId().toString()).toList();
    }

    @Override
    public final PhotoRecord getById(String id) throws InvalidIdException, EntityNotFoundException {
        ObjectId oId;
        try {
            oId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            throw new InvalidIdException(id + "is not a valid ObjectId string");
        }
        return recordRepository.findById(oId).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public PhotoRecord createFromExisting(PhotoRecord newRecord, String previousId, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        PhotoRecord prev = getById(previousId);
        Optional<RecordHistory> history = getRelatedHistory(prev.getId());
        RecordHistory historyRecord = history.orElseGet(() -> new RecordHistory(prev.getId()));
        updateRecordHistory(historyRecord, newRecord, prev);
        discontinue(previousId);
        return addNew(newRecord, username);
    }

    private void updateRecordHistory(RecordHistory recordHistory, PhotoRecord current, PhotoRecord prev) {
        recordHistory.addRecord(prev);
        recordHistory.setCurrent(current.getId());
        historyRepository.save(recordHistory);
    }

    @Override
    public PhotoRecord save(PhotoRecord r) {
        if (!r.getSpecifications().isEmpty()) {
            r.setSpecifications(r.getSpecifications().stream().map(this::saveSpecification).toList());
        }
        return recordRepository.save(r);
    }

    private Specification saveSpecification(Specification specification) {
        specification.setCroppedImg(photoRepository.save(specification.getCroppedImg()));
        return specification;
    }

    @Override
    public PhotoRecord evaluate(PhotoRecord r, String username) {
        PhotoWrapper wrapper;
        try {
            wrapper = photoService.getPhotoWrapperById(r.getPhotoId());
        } catch (EntityNotFoundException e) {
            log.error("Record {} can not be evaluated, because image with id {} does not exist.", r.getId().toString(), r.getPhotoId());
            return r;
        } catch (InvalidIdException e) {
            log.error("Record {} can not be evaluated, because {} is not a valid image id.", r.getId().toString(), r.getPhotoId());
            return r;
        }
        byte[] image = wrapper.getImage();
        File tmp = new File("in.jpg");
        try (OutputStream os = new FileOutputStream(tmp)) {
            os.write(image);
        } catch (IOException e) {
            log.error("Error while writing the temporary file", e);
            tmp.delete();
            return r;
        }
        String projectPath = tmp.getAbsolutePath();
        projectPath = projectPath.substring(0, projectPath.length() - 7);
        Runtime rt = Runtime.getRuntime();
        try {
            Process p = rt.exec(new String[]{"python", projectPath + "/algorithm/cli_app/main.py", "-i", tmp.getAbsolutePath(), "-c", "0.2"});
            if (p.waitFor() == 0) {
                r = PythonScriptUtils.updateRecord(username, r, projectPath, wrapper);
            } else {
                log.error("On no, something went wrong during the run of the python pipeline.");
            }
        } catch (IOException e) {
            log.error("Error executing the record evaluation.", e);
        } catch (InterruptedException e) {
            log.error("Process was interrupted while waiting for the script to stop running.", e);
        } finally {
            tmp.delete();
        }
        photoRepository.save(wrapper);
        save(r);
        return r;
    }

    @Override
    public PhotoRecord addNew(PhotoRecord r, String username) throws EntityNotFoundException, InvalidIdException, EntityAlreadyExistsException {
        Mapping m;
        try {
            m = mappingService.getNewest(username);
        } catch (EntityNotFoundException e) {
            String id = mappingService.createNewMapping(username);
            m = mappingService.getById(id);
        }
        if (recordRepository.getPhotoRecordByPhotoId(r.getPhotoId()).isPresent()) {
            throw new EntityAlreadyExistsException();
        }
        evaluate(r, username);
        m.addRecord(r);
        mappingService.save(m);
        return r;
    }

    @Override
    public void discontinue(String id) throws EntityNotFoundException, InvalidIdException {
        PhotoRecord r = getById(id);
        r.setIsDiscontinued(true);
        recordRepository.save(r);
    }

    @Override
    public int getNumberOfRelated(ObjectId recordId) {
        Optional<RecordHistory> history = getRelatedHistory(recordId);
        return history.map(RecordHistory::getRelatedCount).orElse(0);
    }

    private Optional<RecordHistory> getRelatedHistory(ObjectId recordId) {
        Optional<RecordHistory> history = historyRepository.findByCurrent(recordId);
        if (history.isEmpty()) {
            history = historyRepository.findDistinctByRecordsContaining(recordId);
        }
        return history;
    }

    @Override
    public List<PhotoRecord> getRelated(String id) throws EntityNotFoundException, InvalidIdException {
        PhotoRecord r = getById(id);
        Optional<RecordHistory> history = getRelatedHistory(r.getId());
        if (history.isPresent()) {
            List<PhotoRecord> related = history.get().getRecords();
            related.add(r);
            return related.stream().distinct().toList();
        }
        return Collections.emptyList();
    }
}
