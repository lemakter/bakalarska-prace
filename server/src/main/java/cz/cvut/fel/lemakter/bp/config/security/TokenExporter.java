
package cz.cvut.fel.lemakter.bp.config.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public final class TokenExporter {
    public final JwtDecoder jwtDecoder;

    public String getUsername(Authentication authentication) {
        return (String) ((JwtAuthenticationToken) authentication).getTokenAttributes().get("name");
    }

}
