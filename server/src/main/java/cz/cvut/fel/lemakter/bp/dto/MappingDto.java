package cz.cvut.fel.lemakter.bp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class MappingDto {
    private String id;
    private String note;
    private List<RecordDto> records;
    private long mappingNumber;
}
