package cz.cvut.fel.lemakter.bp.repositories;

import cz.cvut.fel.lemakter.bp.model.account.AccountSettings;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountSettingsRepository extends MongoRepository<AccountSettings, String> {
}
