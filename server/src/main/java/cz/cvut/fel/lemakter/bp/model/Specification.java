package cz.cvut.fel.lemakter.bp.model;

import cz.cvut.fel.lemakter.bp.model.enums.AIResult;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Specification {
    @Id
    private ObjectId id = new ObjectId();
    @DBRef
    private PhotoWrapper croppedImg;
    private AIResult result;

    public Specification(PhotoWrapper croppedImg, AIResult result) {
        this.croppedImg = croppedImg;
        this.result = result;
    }
}
