package cz.cvut.fel.lemakter.bp.dto;

import cz.cvut.fel.lemakter.bp.model.enums.Placement;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class RecordInputDto {
    private Placement placement;
    private String description;
    private String photoId;
}
