package cz.cvut.fel.lemakter.bp.services.impls;

import cz.cvut.fel.lemakter.bp.exceptions.EntityNotFoundException;
import cz.cvut.fel.lemakter.bp.exceptions.InvalidIdException;
import cz.cvut.fel.lemakter.bp.model.Mapping;
import cz.cvut.fel.lemakter.bp.model.PhotoRecord;
import cz.cvut.fel.lemakter.bp.repositories.MappingRepository;
import cz.cvut.fel.lemakter.bp.services.MappingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class MappingServiceImpl implements MappingService {
    private final MappingRepository mappingRepository;

    @Override
    public List<Mapping> findAll(String username) {
        return mappingRepository.getAllByUsername(username);
    }

    @Override
    public Mapping getById(String id) throws EntityNotFoundException, InvalidIdException {
        ObjectId oId;
        try {
            oId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            throw new InvalidIdException(id + "is not a valid ObjectId string");
        }
        return mappingRepository.findById(oId).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public String createNewMapping(String username) {
        Optional<Mapping> curr = mappingRepository.getDistinctByNewestIsTrueAndUsername(username);
        long mappingNumber = 0;
        if (curr.isPresent()) {
            if (curr.get().getRecordsWithDiscontinued().isEmpty()) {
                log.info("Someone tried to create a new mapping without using the existing. We're not doing that.");
                return curr.get().getId().toString();
            } else {
                Mapping m = curr.get();
                m.setNewest(false);
                save(m);
                mappingNumber = m.getMappingNumber() + 1;
            }
        } else {
            log.warn("No mapping marked as 'newest' is present. This is okay, if we're creating the first mapping, but otherwise, this is suspicious.");
        }
        return save(new Mapping(username, mappingNumber)).getId().toString();
    }


    public Mapping save(Mapping mapping) {
        return mappingRepository.save(mapping);
    }

    @Override
    public Mapping getNewest(String username) throws EntityNotFoundException {
        return mappingRepository.getDistinctByNewestIsTrueAndUsername(username).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Optional<Mapping> getMappingContaining(PhotoRecord r) {
        return mappingRepository.getMappingByRecordsContaining(r.getId());
    }
}
