# Welcome to this repository

This repository contains source code for the mobile application for self-examination of skin lestion.

This project is composed of two separate applications, the client and the server.
These applications need to be run separately

## Setup Instructions for Local Development

Tip: If you want to use your own Auth0 server, you need to specify it in the client app in the string.xml file, and also
on the server in the application.properties file.

### Client

To run the client application, first you need to change the address of the server in the RetrofitInstance and
network_security_config.xml file.

The client application can be run by simply building frontend module of the project and running the MainActivity.

### Server

To run the server successfully, you first need to set up the environment for running the python scripts. This needs to
be done according to Readme files in all the modules in the server/algorithm file.

As for the server itself, you need to set-up a Mongo database for the project and set up the correct credentials on
application.properties file.

If the set-up is completed, you can run the server application as a spring boot application from the BPServerApp. 
